<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOpsiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('opsies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('naam');
            $table->integer('onderwerp_id')->unsigned();
            $table->timestamps();

            $table->foreign('onderwerp_id')->references('id')->on('onderwerpe');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('opsies');
    }
}
