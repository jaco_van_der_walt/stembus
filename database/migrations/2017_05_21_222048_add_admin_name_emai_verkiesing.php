<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAdminNameEmaiVerkiesing extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('verkiesings', function ($table) {
            $table->string('admin_naam')->nullable();
            $table->string('admin_selfoon')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('verkiesings', function ($table) {
            $table->dropColumn('admin_naam');
            $table->dropColumn('admin_selfoon');
        });
    }
}
