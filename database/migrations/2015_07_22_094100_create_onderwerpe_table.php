<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOnderwerpeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('onderwerpe', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('verkiesing_id')->unsigned();
            $table->string('naam');
            $table->timestamps();

            $table->foreign('verkiesing_id')->references('id')->on('verkiesings');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('onderwerpe');
    }
}
