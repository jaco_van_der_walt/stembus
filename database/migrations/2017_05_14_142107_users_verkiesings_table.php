<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UsersVerkiesingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('users_verkiesings', function ($table) {
            $table->integer('user_id')->unsigned();
            $table->integer('verkiesing_id')->unsigned();
            $table->boolean('stem');

            $table->foreign('verkiesing_id')->references('id')->on('verkiesings');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users_verkiesings');
    }
}
