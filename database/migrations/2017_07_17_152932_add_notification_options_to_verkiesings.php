<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNotificationOptionsToVerkiesings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('verkiesings', function ($table) {
            $table->boolean('epos_bygevoeg')->default(true);
            $table->boolean('epos_oopmaak')->default(true);
            $table->boolean('sms_oopmaak')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('verkiesings', function ($table) {
            $table->dropColumn('epos_bygevoeg');
            $table->dropColumn('epos_oopmaak');
            $table->dropColumn('sms_oopmaak');
        });
    }
}
