<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUsersKieskolleges extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_kieskolleges', function ($table) {
            $table->integer('user_id')->unsigned();
            $table->integer('kieskollege_id')->unsigned();

            $table->foreign('kieskollege_id')->references('id')->on('kieskolleges');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('users_kieskolleges');
    }
}
