<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVerkiesingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('verkiesings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('naam');
            $table->enum('status', ['oop', 'toe', 'gereed']);
            $table->text('beskrywing');
            $table->timestamp('oopgemaak')->nullable();
            $table->timestamp('gesluit')->nullable();
            $table->integer('persone')->nullable();
            $table->integer('persone_gestem')->nullable();
            $table->integer('aantal_stemme')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('verkiesings');
    }
}
