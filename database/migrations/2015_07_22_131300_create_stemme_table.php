<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStemmeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stemme', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('verkiesing_id')->unsigned();
            $table->integer('onderwerp_id')->unsigned();
            $table->integer('opsie_id')->unsigned();
            $table->string('uuid');
            $table->timestamps();

            $table->foreign('verkiesing_id')->references('id')->on('verkiesings');
            $table->foreign('onderwerp_id')->references('id')->on('onderwerpe');
            $table->foreign('opsie_id')->references('id')->on('opsies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('stemme');
    }
}
