<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(UserTableSeeder::class);

        Model::reguard();
    }
}

class UserTableSeeder extends Seeder {

    public function run()
    {
        DB::table('users')->delete();

        User::create(array('name' => 'Jaco van der Walt', 'password'=>'$2a$10$uS1klqX6R6LH0ZnZUMf24.hCcrOAnMDxB.5u/OrgrcKMdZWUBGflu', 'email'=>'jaco.vn.dr.walt@gmail.com', 'rol'=>'admin', 'stem'=>'false', 'vt_registrasie'=>'V0043'));
        User::create(array('name' => 'Zandra Machin', 'password'=>'$2a$10$f0.ehOgl3j5enoqnzb2CY.M8ejFUiHyh9cpu9/fuY4ykecZRjG1QK', 'email'=>'zandra@voortrekkers.org.za', 'rol'=>'admin', 'stem'=>'false', 'vt_registrasie'=>'V123456'));
    }
}
