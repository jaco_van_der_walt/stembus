<?php

namespace App\Http\Middleware;

use Closure;
use Log;
use Redirect;
use Auth;

class AdminAuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            if ($request->user()->rol != 'admin') {
                return redirect('skakel');
            };
            return $next($request);
        }
        else
        {
            Log::warning("The User is not currently authenticated!");
            return Redirect::to('auth/logout');
        };
    }
}



