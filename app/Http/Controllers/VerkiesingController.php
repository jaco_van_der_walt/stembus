<?php namespace App\Http\Controllers;

use Input;
use Form;
use Validator;
use Redirect;
use Session;
use Response;
use Log;
use Auth;
use View;
use App\Verkiesing;
use App\Onderwerp;
use App\Opsie;
use App\Stem;
use App\User;
use Mail;
use Illuminate\Http\Request;
use App\Jobs\BlockchainCreateNewStream;
use App\Jobs\SendVerkiesingOopEmail;
use App\Jobs\SendVerkiesingOopSMS;

use be\kunstmaan\multichain\MultichainClient;
use be\kunstmaan\multichain\MultichainHelper;

class VerkiesingController extends Controller {

    protected $multichain;
    protected $helper;

	public function __construct()
	{
		$this->middleware('auth');

        $this->multichain = new MultichainClient(getenv('JSON_RPC_URL'), getenv('JSON_RPC_USERNAME'), getenv('JSON_RPC_PASSWORD'), 3);
        $this->helper = new MultichainHelper($this->multichain);

	}

    public function getVerkiesing()
    {
      return View::make('verkiesings_table');
    }

    public function getVerkiesingNuut()
    {
    	return View::make('nuwe_verkiesing');
    }

    public function postVerkiesingNuut()
    {

    	//validation rules
    	$rules = array(
                'verkiesing_naam' => 'Required',
                'verkiesing_beskrywing' => 'Required',
                'onderwerpe' => 'Required',
                'opsies' => 'Required',
                'admin_naam' => 'Required',
                'admin_selfoon' => 'Required',
            );
    	$v = Validator::make(Input::all(), $rules);

    	if( $v->passes() ) {

            $verkiesing = new Verkiesing;

            $verkiesing->naam = Input::get('verkiesing_naam');
            $verkiesing->status = "gereed";
            $verkiesing->beskrywing = Input::get('verkiesing_beskrywing');
            $verkiesing->admin_naam = Input::get('admin_naam');
            $verkiesing->admin_selfoon = Input::get('admin_selfoon');
            $verkiesing->stream = uniqid();

            //Notification Settings for this Verkiesing
            if(Input::get('epos_bygevoeg'))
            {
                $verkiesing->epos_bygevoeg = true;
            } else
            {
                $verkiesing->epos_bygevoeg = false;
            }

            if(Input::get('epos_oopmaak'))
            {
                $verkiesing->epos_oopmaak = true;
            } else
            {
                $verkiesing->epos_oopmaak = false;
            }

            if(Input::get('sms_oopmaak'))
            {
                $verkiesing->sms_oopmaak = true;
            } else
            {
                $verkiesing->sms_oopmaak = false;
            }

            $verkiesing->save();

            $onderwerpe = Input::get('onderwerpe');
            $opsies = Input::get('opsies');

            $i = 0;
            foreach($onderwerpe as $o)
            {
                $onderwerp = new Onderwerp;
                $onderwerp->naam = $o;
                $onderwerp->verkiesing_id = $verkiesing->id;
                $onderwerp->save();

                $onderwerp_opsies = preg_split('/([,])+/', $opsies[$i], -1, PREG_SPLIT_NO_EMPTY);
                foreach($onderwerp_opsies as $op)
                {
                    $opsie = new Opsie;
                    $opsie->naam = $op;
                    $opsie->onderwerp_id = $onderwerp->id;
                    $opsie->save();

                }
                $i++;

            }
            return Redirect::to('verkiesings')->with('success', $verkiesing->naam." is bygevoeg");

    	}
    		else {
                //Invalid input! Redirecting back...
                return redirect()->back()->withInput()->withErrors($v);
        }

    	return redirect()->back()->withInput()->withErrors($v);
    }


    public function ajax_verkiesings()
    {

        $return['recordsTotal'] = Verkiesing::count();

        $flashdata = [];
        $return['data'] = [];
        $i = 0;
        foreach (Verkiesing::orderBy('created_at')->get() as $verkiesing) {
            $return['data'][$i] = [$verkiesing->naam];
            $return['data'][$i][] = [$verkiesing->stream];
            $return['data'][$i][] = [$verkiesing->created_at->format('Y-m-d H:i:s')];
            if($verkiesing->status === 'oop')
            {
            	$return['data'][$i][] =  '<button class="btn btn-xs bg-olive disabled"><i class="fa fa-unlock"></i> Oop</button>';
                $return['data'][$i][] = '<i class="fa fa-users"></i>  '.$verkiesing->kieskollege->count();
                $return['data'][$i][] = '<i class="fa fa-users"></i>  '.$verkiesing->kieskollege()->wherePivot('stem',true)->count();
                $return['data'][$i][] =  '<i class="fa fa-flag"></i>  '.Stem::where('verkiesing_id', $verkiesing->id)->count();
                $return['data'][$i][] =  '<a href="' . url("verkiesings/voorskou/$i") . '" class="btn btn-xs blue" target="_blank"><i class="fa fa-eye"></i> Voorskou</a> <a href="' . url("verkiesings/kieskollege/$i") . '" class="btn btn-xs blue"><i class="fa fa-users"></i> Kieskollege</a> <a href="' . url("boodskap/kieskollege/$verkiesing->id") . '" class="btn btn-xs blue"><i class="fa fa-envelope-o"></i> Boodskap</a>  <a href="#" class="btn btn-xs blue" data-toggle="modal" data-target="#sluitVerkiesingModal"><i class="fa fa-lock"></i> Sluit</a>';
            }
            else if ($verkiesing->status === 'toe') {



                if( strtotime($verkiesing->gesluit) < strtotime('-7 day') ) {
                    $return['data'][$i][] =  '<button class="btn btn-xs bg-red-pink disabled"><i class="fa fa-lock"></i> Gesluit</button>';
                    $return['data'][$i][] =  '<i class="fa fa-users"></i>  '.$verkiesing->kieskollege->count();
                    $return['data'][$i][] =  '<i class="fa fa-users"></i>  '.$verkiesing->kieskollege()->wherePivot('stem',true)->count();
                    $return['data'][$i][] =  '<i class="fa fa-flag"></i>  '.Stem::where('verkiesing_id', $verkiesing->id)->count();
                    $return['data'][$i][] =  '<i>Verkiesing in Argief</i>';
                }
                else{
                	$return['data'][$i][] =  '<button class="btn btn-xs bg-red-pink disabled"><i class="fa fa-lock"></i> Gesluit</button>';
                    $return['data'][$i][] =  '<i class="fa fa-users"></i>  '.$verkiesing->kieskollege->count();
                    $return['data'][$i][] =  '<i class="fa fa-users"></i>  '.$verkiesing->kieskollege()->wherePivot('stem',true)->count();
                    $return['data'][$i][] =  '<i class="fa fa-flag"></i>  '.Stem::where('verkiesing_id', $verkiesing->id)->count();
                    $return['data'][$i][] =  '<a href="' . url("verkiesings/voorskou/$i") . '" class="btn btn-xs blue" target="_blank"><i class="fa fa-eye"></i> Voorskou</a> <a href="' . url("verkiesings/kieskollege/$i") . '" class="btn btn-xs blue"><i class="fa fa-users"></i> Kieskollege</a> <a href="' . url("boodskap/kieskollege/$verkiesing->id") . '" class="btn btn-xs blue"><i class="fa fa-envelope-o"></i> Boodskap</a><a href="' . url("verkiesings/resultaat/$i") . '" class="btn btn-xs blue"><i class="fa fa-bar-chart"></i> Resultaat</a>';
                };
            }
            else if ($verkiesing->status === 'gereed')
            {
            	$return['data'][$i][] =  '<button class="btn btn-xs bg-blueish disabled"><i class="fa fa-thumbs-o-up"></i> Gereed</button>';
                $return['data'][$i][] =  '<i class="fa fa-users"></i>  '.$verkiesing->kieskollege->count();
                $return['data'][$i][] =  '';
                $return['data'][$i][] =  '';
                $return['data'][$i][] =  '<a href="' . url("verkiesings/voorskou/$i") . '" class="btn btn-xs blue" target="_blank"><i class="fa fa-eye"></i> Voorskou</a> <a href="' . url("verkiesings/kieskollege/$i") . '" class="btn btn-xs blue"><i class="fa fa-users"></i> Kieskollege</a> <a href="' . url("boodskap/kieskollege/$verkiesing->id") . '" class="btn btn-xs blue"><i class="fa fa-envelope-o"></i> Boodskap</a><a href="' . url("verkiesings/skrap/$i") . '" class="btn btn-xs blue"><i class="fa fa-trash-o"></i> Skrap</a> <a href="' . url("verkiesings/maakoop/$i") . '" class="btn btn-xs blue"><i class="fa fa-paper-plane"></i> Maak Oop</a>';
            }
            else
            {
            	$return['data'][$i][] = "onbekend";
            }


            $flashdata[$i] = $verkiesing->id;
            $i++;
        }

        Session::set('verkiesing', $flashdata);

        return response()->json($return);
    }

    public function getSkrap($id)
    {
    	$real_id = Session::get("verkiesing.$id", false);
    	$verkiesing = Verkiesing::where('id',$real_id)->first();

    	if(!$verkiesing)
    	{
    		 return Redirect::to('verkiesings')->withErrors("Kon nie die verkiesing skrap nie!");
    	}

        //Only allowed to delete a verkiesing if it hasn't opened
        if($verkiesing->status != 'gereed')
        {
            return Redirect::to('verkiesings')->withErrors("Die verkiesing kan nie meer geskrap word nie!");
        }

        //Detach all users from this verkiesing
        $users = $verkiesing->kieskollege;
        foreach($users as $u)
        {
            $verkiesing->kieskollege()->detach($u);
        }

        //Delete all opsies and onderwerpe
    	foreach($verkiesing->onderwerpe as $onderwerp)
    		{
    			foreach($onderwerp->opsies as $opsie)
    			{
    				$opsie->delete();
    			}
    			$onderwerp->delete();
    		}

    	$verkiesing->delete();

    	return Redirect::to('verkiesings')->with('success', $verkiesing->naam." is geskrap");
    }

    public function getMaakOop($id)
    {

        $real_id = Session::get("verkiesing.$id", false);
        $verkiesing = Verkiesing::where('id', $real_id)->first();


        if(!$verkiesing)
        {
             return Redirect::to('verkiesings')->withErrors("Kon nie die verkiesing oopmaak nie!");
        }

        //Maak seker dat almal in die kieskollege kan stem
        $ids = $verkiesing->kieskollege()->getRelatedIds();
        foreach ($ids as $id){
            $verkiesing->kieskollege()->updateExistingPivot($id, ['stem' => false]);
        }

        $oop_count = Verkiesing::where('status', 'oop')->count();

        if($oop_count != 0)
        {
            return Redirect::to('verkiesings')->withErrors("Daar is reeds 'n ander oop verkiesing!");
        }
        else
        {
            $verkiesing->status = 'oop';
            $verkiesing->oopgemaak = date('Y-m-d H:i:s');
            //$verkiesing->persone_gestem = User::where('rol', 'stem')->where('stem','1')->count();
            $verkiesing->aantal_stemme = Stem::where('verkiesing_id', $verkiesing->id)->count();
            $verkiesing->save();

            //Write Verkiesing stream to Blockchain
            $this->dispatch(new BlockchainCreateNewStream($verkiesing));

            $users = User::all();

            foreach($users as $user)
            {
                //Only send to users in kieskollege
                if($verkiesing->kieskollege->contains($user))
                {

                    //Generate a new password for this user
                    $bytes = openssl_random_pseudo_bytes(3, $cstrong);
                    $hex   = bin2hex($bytes);
                    $wagwoord = strtoupper($hex);
                    $wagwoord = str_replace("0","3",$wagwoord);
                    $user->wagwoord = $wagwoord;
                    $user->password = bcrypt($wagwoord);
                    $user->save();

                    //Notifications
                    if($verkiesing->epos_oopmaak == '1')
                    {
                        Log::info("Queued: Verkiesing Oop email to ".$user->email);
                        //Dispatch email
                        $this->dispatch(new SendVerkiesingOopEmail($user, $verkiesing));
                    }
                    if($verkiesing->sms_oopmaak == '1')
                    {
                       Log::info("Queued: Verkiesing Oop SMS to ".$user->selfoon);
                       //Dispatch sms
                       $this->dispatch(new SendVerkiesingOopSMS($user, $verkiesing));
                    }
                }
            }
            Log::notice("'n Verkiesing is oopgemaak", ['admin_id' => Auth::user()->id, 'admin_name'=>  Auth::user()->name, 'admin_email' => Auth::user()->email, 'verkiesing_id' => $verkiesing->id, 'verkiesing_name' => $verkiesing->naam]);

            return Redirect::to('verkiesings')->with('success', $verkiesing->naam." is nou oop!");
        }
    }

    public function getSluit()
    {
        $verkiesing = Verkiesing::where('status', 'oop')->first();

        if(!$verkiesing)
        {
             return Redirect::to('verkiesings')->withErrors("Kon nie die verkiesing sluit nie!");
        }

        $verkiesing->status= 'toe';
        $verkiesing->gesluit = date('Y-m-d H:i:s');
        $verkiesing->save();

        Log::notice("'n Verkiesing is gesluit", ['admin_id' => Auth::user()->id, 'admin_name'=>  Auth::user()->name, 'admin_email' => Auth::user()->email, 'verkiesing_id' => $verkiesing->id, 'verkiesing_name' => $verkiesing->naam]);

        return Redirect::to('verkiesings')->with('success', $verkiesing->naam." is gesluit");
    }

    public function getResultaat($id)
    {
        $real_id = Session::get("verkiesing.$id", false);
        $verkiesing = Verkiesing::where('id', $real_id)->first();

        if(!$verkiesing)
        {
             return Redirect::to('verkiesings')->withErrors("Kon nie die verkiesing resultaat kry nie nie!");
        }

        $telling = array();
        $opsies = array();
        foreach($verkiesing->onderwerpe as $onderwerp)
        {
            foreach($onderwerp->opsies as $opsie)
            {
                $opsies[$onderwerp->id][] = $opsie;
                $telling[$opsie->id] = Stem::where('opsie_id', $opsie->id)->count();
            }
        }
        $aantal_stemme = Stem::where('verkiesing_id', $verkiesing->id)->count();
        $verkiesing_stemme = Stem::where('verkiesing_id', $verkiesing->id)->orderBy('uuid')->get();
        $stemme = array();

        $i = 0;
        foreach($verkiesing_stemme as $stem)
        {
            $stemme[$i] = $stem;
            $i++;
        }

        return view('verkiesing_resultaat')->with(['verkiesing' => $verkiesing, 'onderwerpe' => $verkiesing->onderwerpe, 'opsies' => $opsies, 'telling' => $telling, 'stemme' => $stemme, 'aantal_stemme' => $aantal_stemme]);

    }

    public function getVoorskou($id)
    {
        $real_id = Session::get("verkiesing.$id", false);
        $verkiesing = Verkiesing::where('id', $real_id)->first();

      $opsies = array();
        foreach($verkiesing->onderwerpe as $onderwerp)
        {
            foreach($onderwerp->opsies as $opsie)
            {
                $opsies[$onderwerp->id][] = $opsie;
            }
        }
        return View::make('stembrief_voorskou')->with(['verkiesing' => $verkiesing, 'onderwerpe' => $verkiesing->onderwerpe, 'opsies' => $opsies]);
    }

    public function getKieskollege($id)
    {
        $real_id = Session::get("verkiesing.$id", false);
        $verkiesing = Verkiesing::where('id', $real_id)->first();
        Session::set("kieskollege_verkiesing", $verkiesing);
        return View::make('kieskollege')->with(['verkiesing' => $verkiesing]);
    }

}
