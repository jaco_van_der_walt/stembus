<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use App\User;
use App\Verkiesing;
use App\Kieskollege;
use App\Jobs\SendKieskollegeAddedEmail;
use Input;
use Log;
use Form;
use Validator;
use Redirect;
use Auth;
use App\Jobs\SendKieskollegeBoodskapEmail;
use App\Jobs\SendKieskollegeBoodskapSMS;

class KieskollegeController extends Controller
{
    public function getVoegBy($id)
    {
        Log::info('Attempting to add a registered user to a Stemming Kieskollege');
        $user = User::where('id', '=', $id)->first();

        if($user == '')
            abort(500, 'Persoon nie gevind nie');

        $verkiesing = Session::get("kieskollege_verkiesing");
        if($verkiesing == '')
            abort(500, 'Verkiesing nie gevind nie');

        if($verkiesing->kieskollege->contains($user->id))
            abort(403, 'Die persoon is reeds in die Kieskollege');

        $verkiesing->kieskollege()->attach($user->id);
        Log::info("A User has been added to a Verkiesing", ['user_id' => $user->id, 'user_name'=> $user->name, 'user_email' => $user->email, 'verkiesing_id' => $verkiesing->id, 'verkiesing_name' => $verkiesing->naam]);

        //Notification to User
        if($verkiesing->epos_bygevoeg == '1')
        {
            $this->dispatch(new SendKieskollegeAddedEmail($user, $verkiesing));
            Log::info("Queued: Kieskollege Added email", ['user_id' => $user->id, 'user_name'=> $user->name, 'user_email' => $user->email, 'verkiesing_id' => $verkiesing->id, 'verkiesing_name' => $verkiesing->naam]);
        };

        return redirect()->back();

    }

    public function getSkrapPersoon($id)
    {
        $verkiesing = Session::get('kieskollege_verkiesing');
        $user = User::where('id', '=', $id)->withTrashed()->first();
        if($user == '')
            abort(500, 'Persoon nie gevind nie');

        $verkiesing->kieskollege()->detach($user->id);
        Log::info("A User has been removed from a Verkiesing", ['user_id' => $user->id, 'user_name'=> $user->name, 'user_email' => $user->email, 'verkiesing_id' => $verkiesing->id, 'verkiesing_name' => $verkiesing->naam]);
        return redirect()->back();
    }

    public function getAjaxKieskollege($id)
    {

        $verkiesing = Verkiesing::find($id);
        $return['recordsTotal'] = $verkiesing->kieskollege->count();

        $return['data'] = [];
        $i = 0;

        foreach ($verkiesing->kieskollege as $persoon) {
            $return['data'][$i] = [$persoon->name];
            $return['data'][$i][] = [$persoon->email];
            $return['data'][$i][] = [$persoon->selfoon];

            if($verkiesing->status == "oop")
                $return['data'][$i][] = [$persoon->wagwoord];
            else
                $return['data'][$i][] = "";

            if($verkiesing->status == "gereed")
                $return['data'][$i][] =  '<a href="' . url("kieskollege/verwyder/$persoon->id") . '" class="btn btn-xs blue"><i class="fa fa-user-times"></i> Verwyder</a>';
            else
                $return['data'][$i][] = "";
            $i++;
        }
        
        return response()->json($return);
    }

    public function getBoodskap($id)
    {
        $verkiesing = Verkiesing::find($id);
        $kieskollege = $verkiesing->kieskollege->count();
        return view('boodskap')->with(['verkiesing_id' => $verkiesing->id]);
    }

    public function postBoodskap($id)
    {
        //validation rules
        $rules = array(
                'boodskap' => 'Required',
            );
        $v = Validator::make(Input::all(), $rules);

        if( $v->passes() ) {

            $verkiesing = Verkiesing::find($id);
            $users = User::all();
            $sms = Input::get('sms');
            $email = Input::get('epos');
            $boodskap = Input::get('boodskap');

            foreach($users as $user)
            {
                //Only send to users in kieskollege
                if($verkiesing->kieskollege->contains($user))
                {
                    if($sms)
                    {
                        $this->dispatch(new SendKieskollegeBoodskapSMS($user, $verkiesing, $boodskap));
                        Log::info("Queued: Boodskap sms '".$boodskap."' to ".$user->selfoon);

                    };

                    if($email)
                    {
                        $this->dispatch(new SendKieskollegeBoodskapEmail($user, $verkiesing, $boodskap));
                        Log::info("Queued: Boodskap e-mail '".$boodskap."' to ".$user->email);
                    }

                }
            }
            return Redirect::to('verkiesings')->with('success', "Boodskap is gestuur na Kieskollege");
        }
        else
        {
            return redirect()->back()->withInput()->withErrors($v);
        }
    }

    public function getTable()
    {
        return view('kieskollegestable');
    }

    public function ajax_kieskolleges()
    {
        $kieskolleges = Kieskollege::all();
        $return['recordsTotal'] = Kieskollege::all()->count();
        $flashdata = [];
        $return['data'] = [];
        $i = 0;

        foreach ($kieskolleges as $k) {
            $return['data'][$i] = [$k->naam];
            $return['data'][$i][] = ['<i class="fa fa-users"></i>  '.$k->users()->count()];
            $return['data'][$i][] =  '<a href="' . url("kieskollege/view/$k->id") . '" class="btn btn-xs blue"><i class="fa fa-eye"></i> Beskou</a>';
            $i++;
        }
        return response()->json($return);
    }

    public function postKieskollegeNuut()
    {
        $rules = array(
                'kieskollege_naam' => 'Required',
            );
        $v = Validator::make(Input::all(), $rules);
        if( $v->passes() ) {
            $kieskollege = new Kieskollege();
            $kieskollege->naam = Input::get('kieskollege_naam');
            $kieskollege->save();
            return Redirect::to('kieskollege/table')->with('success', "Kieskollege '".$kieskollege->naam."' is geskep");
        }
        else
        {
           return redirect()->back()->withInput()->withErrors($v);
        }
    }

    public function getViewKieskollege($id)
    {
        $kieskollege = Kieskollege::find($id);
        return view('kieskollegebeskou')->with(['kieskollege' => $kieskollege]);
    }

    public function ajax_kieskollege_users($id)
    {
        $kieskollege = Kieskollege::find($id);
        $users = $kieskollege->users()->get();
        $return['recordsTotal'] = $users->count();
        $flashdata = [];
        $return['data'] = [];
        $i = 0;

        foreach ($users as $k) {
            $return['data'][$i] = [$k->name];
            $return['data'][$i][] = [$k->email];
            $return['data'][$i][] = [$k->selfoon];
            $return['data'][$i][] =  '<a href="' . url("kieskollege/$kieskollege->id/verwyder/$k->id") . '" class="btn btn-xs blue"><i class="fa fa-user-times"></i> Verwyder</a>';
            $i++;
        }
        return response()->json($return);
    }

    public function ajax_kieskollege_add_users($id)
    {
        $return['recordsTotal'] = User::count();

        $kieskollege = Kieskollege::find($id);

        $return['data'] = [];
        $i = 0;

        foreach (User::where('rol', '=', 'stem')->orderBy('name')->get() as $persoon) {
            $return['data'][$i] = [$persoon->name];
            $return['data'][$i][] = [$persoon->email];
            $return['data'][$i][] = [$persoon->selfoon];

            if($kieskollege->users->contains($persoon->id))
              $return['data'][$i][] = 'Reeds in '.$kieskollege->naam;
            else
              $return['data'][$i][] =  '<a href="' . url("kieskollege/$kieskollege->id/voeg_by/$persoon->id") . '" class="btn btn-xs blue"><i class="fa fa-plus"></i> Voeg By</a>';

            $i++;
        }
        return response()->json($return);
    }

    public function getVoegUserByKieskollege($kieskollege_id, $user_id)
    {
        $kieskollege = Kieskollege::find($kieskollege_id);
        $user = User::find($user_id);

        $kieskollege->users()->attach($user);

        return Redirect::to('kieskollege/view/'.$kieskollege->id)->with('success', "$user->name is by $kieskollege->naam gevoeg");
    }

    public function getVerwyderUserByKieskollege($kieskollege_id, $user_id)
    {
        $kieskollege = Kieskollege::find($kieskollege_id);
        $user = User::find($user_id);

        $kieskollege->users()->detach($user);

        return Redirect::to('kieskollege/view/'.$kieskollege->id)->with('success', "$user->name is verwyder uit $kieskollege->naam");
    }

    public function getSkrapKieskollege($id)
    {
        $kieskollege = Kieskollege::find($id);
        $naam = $kieskollege->naam;

        foreach($kieskollege->users as $user)
        {
            $kieskollege->users()->detach($user);
        }

        $kieskollege->delete();
        return Redirect::to('kieskollege/table')->with('success', "Kieskollege '".$naam."' is geskrap");
    }

    public function getLaaiKieskollege($verkiesing_id)
    {
        $kieskolleges = Kieskollege::all();
        $return['recordsTotal'] = Kieskollege::all()->count();
        $flashdata = [];
        $return['data'] = [];
        $i = 0;

        foreach ($kieskolleges as $k) {
            $return['data'][$i] = [$k->naam];
            $return['data'][$i][] = ['<i class="fa fa-users"></i>  '.$k->users()->count()];
            $return['data'][$i][] =  '<a href="' . url("kieskollege/$k->id/laai/$verkiesing_id") . '" class="btn btn-xs blue"><i class="fa fa-upload"></i> Laai</a>';
            $i++;
        }
        return response()->json($return);
    }

    public function getLaaiKieskollegeVerkiesing($kieskollege_id, $verkiesing_id)
    {
        $kieskollege = Kieskollege::find($kieskollege_id);
        $verkiesing = Verkiesing::find($verkiesing_id);

        foreach($kieskollege->users as $user)
        {
            if(!$verkiesing->kieskollege->contains($user))
            {
                $verkiesing->kieskollege()->attach($user);
                //Notify User
                if($verkiesing->epos_bygevoeg == '1')
                {
                    $this->dispatch(new SendKieskollegeAddedEmail($user, $verkiesing));
                    Log::info("Queued: Kieskollege Added email", ['user_id' => $user->id, 'user_name'=> $user->name, 'user_email' => $user->email, 'verkiesing_id' => $verkiesing->id, 'verkiesing_name' => $verkiesing->naam]);
                    Log::notice("A User has been added to a Verkiesing through a Kieskollege import", ['user_id' => $user->id, 'user_name'=> $user->name, 'user_email' => $user->email, 'verkiesing_id' => $verkiesing->id, 'verkiesing_name' => $verkiesing->naam, 'kieskollege_id' => $kieskollege->id, 'kieskollege_naam' => $kieskollege->naam]);
                }
            }
        }
        return Redirect::back()->with('success', "Kieskollege '$kieskollege->naam' is gelaai vir verkiesing '$verkiesing->naam'");
    }

    public function drukKieskollege($id)
    {
      $kieskollege = Kieskollege::find($id);
      return view('kieskollege_druk')->with(['kieskollege' => $kieskollege]);
    }
}
