<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use be\kunstmaan\multichain\MultichainClient;
use be\kunstmaan\multichain\MultichainHelper;

use App\Stem;
use App\Verkiesing;

class BlockchainController extends Controller
{
    protected $multichain;
    protected $helper;

    public function __construct()
    {
        $this->multichain = new MultichainClient(getenv('JSON_RPC_URL'), getenv('JSON_RPC_USERNAME'), getenv('JSON_RPC_PASSWORD'), 3);
        $this->helper = new MultichainHelper($this->multichain);
    }

    public function getStemSoek($id)
    {
        $stem = Stem::where('uuid',$id)->first();
        if($stem->tx == '')
            return "Stem nog nie in Blokketting geskryf nie";
        $verkiesing = Verkiesing::where('id',$stem->verkiesing_id)->first();
        return $this->multichain->getStreamItem($verkiesing->stream, $stem->tx);

    }

    public function getBlockchainInfo()
    {
        return $this->multichain->getInfo();
    }
}
