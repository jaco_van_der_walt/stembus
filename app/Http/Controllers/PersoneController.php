<?php namespace App\Http\Controllers;

use Input;
use Form;
use Validator;
use Redirect;
use Session;
use Response;
use Log;
use Auth;
use View;
use App\User;
use App\Verkiesing;
use App\Kieskollege;
use Mail;

class PersoneController extends Controller {

	public function __construct()
	{
		$this->middleware('auth');
	}


    public function getPersone()
    {
      $oop_verkiesing_count = Verkiesing::where('status','oop')->count();

      return View::make('persone_table')->with(['oop_verkiesing_count' => $oop_verkiesing_count]);
    }

    public function ajax_persone()
    {

        $return['recordsTotal'] = User::count();

        $flashdata = [];
        $return['data'] = [];
        $i = 0;

        $oop_verkiesing_count = Verkiesing::where('status','oop')->count();
        foreach (User::where('rol', '=', 'stem')->orderBy('name')->get() as $persoon) {
            $return['data'][$i] = [$persoon->name];
            $return['data'][$i][] = [$persoon->vt_registrasie];
            $return['data'][$i][] = [$persoon->email];
            $return['data'][$i][] = [$persoon->selfoon];

            if($oop_verkiesing_count != 0)
            {
              $return['data'][$i][] ="";
            } else
            {
              $return['data'][$i][] =  '<a href="' . url("persoon/skrap/$i") . '" class="btn btn-xs blue"><i class="fa fa-trash-o"></i> Skrap</a>';
            }

            $flashdata[$i] = $persoon->id;
            $i++;
        }

        Session::set('persoon', $flashdata);

        return response()->json($return);
    }

		//This is where the bug is!!
    public function ajax_kieskollege_persone()
    {
        $return['recordsTotal'] = User::count();

        $verkiesing = Session::get("kieskollege_verkiesing");

        $return['data'] = [];
        $i = 0;

        foreach (User::where('rol', '=', 'stem')->orderBy('name')->get() as $persoon) {
            $return['data'][$i] = [$persoon->name];
            $return['data'][$i][] = [$persoon->email];
            $return['data'][$i][] = [$persoon->selfoon];

						//This causes a bug
            //if($verkiesing->kieskollege->contains($persoon->id))
              //$return['data'][$i][] = 'Reeds in Kieskollege';
            //else
              $return['data'][$i][] =  '<a href="' . url("stemming/kieskollege/voegby/$persoon->id") . '" class="btn btn-xs blue"><i class="fa fa-plus"></i> Voeg By</a>';

            $i++;
        }

        return response()->json($return);
    }


    public function postVoegby(){
      $naam = Input::get('naam');
      $vt_registrasie = Input::get('vt_registrasie');
      $epos = Input::get('epos');
      $selfoon = Input::get('selfoon');

      if(($naam == '') || ($epos == '') || ($selfoon == ''))
        return Redirect::to('persone')->withErrors("Die persoon kon nie bygevoeg word nie");
      else
      {
        if (User::where('email', '=', $epos)->first())
        {
          return Redirect::to('persone')->withErrors("'n Persoon met die e-posadres ".$epos." is reeds geregistreer!");
        }
        else if (User::withTrashed()->where('email', "=", $epos)->first()) {
          //Check if user isn't maybe in the deleted list
          $user = User::withTrashed()->where('email', "=", $epos)->first();
          $user->restore();
					$user->name = $naam;
          $user->rol = "stem";
          $user->vt_registrasie = $vt_registrasie;
          $user->selfoon = $selfoon;
          $user->rol = "stem";

          $bytes = openssl_random_pseudo_bytes(3, $cstrong);
          $hex   = bin2hex($bytes);
          $wagwoord = strtoupper($hex);

          $user->wagwoord = $wagwoord;
          $user->password = bcrypt($wagwoord);

          $user->save();

          Log::notice("'n User wat voorheen geskrap was is weer geskep", ['admin_id' => Auth::user()->id, 'admin_name'=>  Auth::user()->name, 'admin_email' => Auth::user()->email, 'user_id' => $user->id, 'user_name' => $user->name]);

          return Redirect::to('persone')->with('success', $user->name." is gestoor");
        }
        else { //new user - add
          $bytes = openssl_random_pseudo_bytes(3, $cstrong);
          $hex   = bin2hex($bytes);
          $wagwoord = strtoupper($hex);

          $user = new User;
          $user->name = $naam;
          $user->vt_registrasie = $vt_registrasie;
          $user->email = $epos;
          $user->selfoon = $selfoon;
          $user->rol = "stem";
          $user->wagwoord = $wagwoord;
          $user->password = bcrypt($wagwoord);

          $user->save();

          Log::notice("'n nuwe User is geskep", ['admin_id' => Auth::user()->id, 'admin_name'=>  Auth::user()->name, 'admin_email' => Auth::user()->email, 'user_id' => $user->id, 'user_name' => $user->name]);

          return Redirect::to('persone')->with('success', $user->name." is gestoor");
        }
        return Input::all();
      }
    }

    public function getSkrap($id)
    {

      $oop_verkiesing_count = Verkiesing::where('status','oop')->count();

      if($oop_verkiesing_count != 0)
      {
        return Redirect::to('persone')->withErrors("Daar is tans 'n oop verkiesing en persone kan nie geskrap word nie!");
      }

      $real_id = Session::get("persoon.$id", false);
      $user = User::where('id', '=', $real_id)->first();
      $user_id = $user->id;
      $user_name = $user->name;
      $user_email = $user->email;

      //Delete User from any Kieskollege
      $kieskolleges = Kieskollege::all();
      foreach($kieskolleges as $k)
      {
        if ($k->users->contains($user))
        {
          $k->users()->detach($user);
        }
      }

      //Delete User from any Verkiesing that's GEREED
      $verkiesings = Verkiesing::where('status','gereed')->get();
      foreach($verkiesings as $v)
      {
        if($v->kieskollege->contains($user))
        {
          $v->kieskollege()->detach($user);
        }
      }

      $user->delete();

      Log::notice("'n User is geskrap", ['admin_id' => Auth::user()->id, 'admin_name'=>  Auth::user()->name, 'admin_email' => Auth::user()->email, 'user_id' => $user_id, 'user_name' => $user_name, 'user_email' => $user_email]);
      return Redirect::to('persone')->with('success', $user->name." is geskrap");
    }

    public function getSkrapAlmal()
    {

      $oop_verkiesing_count = Verkiesing::where('status','oop')->count();

      if($oop_verkiesing_count != 0)
      {
        return Redirect::to('persone')->withErrors("Daar is tans 'n oop verkiesing en persone kan nie geskrap word nie!");
      }

      foreach (User::where('rol', '=', 'stem')->get() as $persoon) {
        $persoon->delete();
      }

      return Redirect::to('persone')->with('success', "Alle persone is van die stemregister geskrap");
    }

    public function getHerstelStemreg()
    {
      $oop_verkiesing_count = Verkiesing::where('status','oop')->count();

      if($oop_verkiesing_count != 0)
      {
        return Redirect::to('persone')->withErrors("Daar is tans 'n oop verkiesing en persone se stemreg kan nie herstel word nie!");
      }

      foreach (User::where('rol', '=', 'stem')->get() as $persoon) {
        $persoon->stem = '0';
        $persoon->save();
      }

      return Redirect::to('persone')->with('success', "Alle persone se stemreg is herstel");
    }

}
