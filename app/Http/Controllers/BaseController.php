<?php namespace App\Http\Controllers;

use Input;
use Form;
use Validator;
use Redirect;
use Session;
use Response;
use Log;
use Auth;
use View;
use App\Verkiesing;
use App\Onderwerp;
use App\Opsie;
use App\User;
use Mail;
use Illuminate\Http\Request;


class BaseController extends Controller {

	public function __construct()
	{
		$this->middleware('auth');
	}

    public function router()
    {
        //Stuur die persoon wat ingeteken het na regte plek
        $user = Auth::user();

        if($user->rol === 'admin')
        {
            Log::info("Redirecting user to admin controller", ['user_id'=> $user->id, 'user_name' => $user->name, 'user_email' => $user->email]);
            return Redirect::to('admin');
        }
        else if($user->rol === 'stem')
        {
            Log::info("Redirecting user to stem controller", ['user_id'=> $user->id, 'user_name' => $user->name, 'user_email' => $user->email]);
            return Redirect::to('stem');
        }

        Auth::logout();
        return abort(500);
    }

    public function getStem()
    {
      $verkiesings_count = Verkiesing::where('status', 'oop')->count();

      if($verkiesings_count == 0)
      {
        return Redirect::to('stem/geen');
      }

      $verkiesing = Verkiesing::where('status', 'oop')->orderBy('updated_at')->first();

      if($verkiesing->kieskollege->contains(Auth::user())) //User is in die Kieskollege vir die Verkiesing
      {

        $kieskollege = $verkiesing->kieskollege()->where("user_id", Auth::user()->id)->where("verkiesing_id", $verkiesing->id)->first();
        if($kieskollege->pivot->stem === 1)
        {
          Log::warning("User already voted in this Verkiesing.", ['user_id'=> Auth::user()->id, 'user_name' => Auth::user()->name, 'verkiesing_id' => $verkiesing->id, 'verkiesing_naam' => $verkiesing->naam]);
          return Redirect::to('stem/reedsvoltooi');
        }
        
        $opsies = array();
        foreach($verkiesing->onderwerpe as $onderwerp)
        {
            foreach($onderwerp->opsies as $opsie)
            {
                $opsies[$onderwerp->id][] = $opsie;
            }
            
        }
        Session::set('huidige_verkiesing', $verkiesing->id);
        Log::notice("User is authorized. Showing Stembrief to User.", ['user_id'=> Auth::user()->id, 'user_name' => Auth::user()->name, 'verkiesing_id' => $verkiesing->id, 'verkiesing_naam' => $verkiesing->naam]);
        return View::make('stembrief')->with(['verkiesing' => $verkiesing, 'onderwerpe' => $verkiesing->onderwerpe, 'opsies' => $opsies]);
      }
      else
      {
        Log::warning("There are no open Verkiesings for this User.", ['user_id'=> Auth::user()->id, 'user_name' => Auth::user()->name, 'verkiesing_id' => $verkiesing->id, 'verkiesing_naam' => $verkiesing->naam]);
        return Redirect::to('stem/geen');
      }

     
    }

}