<?php namespace App\Http\Controllers;

use Input;
use Form;
use Validator;
use Redirect;
use Session;
use Response;
use Log;
use Auth;
use View;
use App\Verkiesing;
use App\Onderwerp;
use App\Opsie;
use App\Stem;
use App\User;
use App\Jobs\BlockchainCreateNewStreamItem;

class StemController extends Controller {

	public function __construct()
	{
		$this->middleware('auth');
	}

  public function postStem()
    {

      $verkiesing_id = Session::get('huidige_verkiesing');

      $verkiesing = Verkiesing::where('id', $verkiesing_id)->first();

      //Stoor die verkiesing data dadelik in die sessie
      foreach($verkiesing->onderwerpe as $onderwerp)
      {
        if(Input::get($onderwerp->id) == "")
        {
          return Redirect::to('skakel')->withErrors("Daar was 'n probleem met U stembrief");
        }
        else
        {
          Session::set($verkiesing->id."_".$onderwerp->id,Input::get($onderwerp->id));
        }
      };

      //Om te verseker ons stem vir die korrekte opsie, gaan ons dit nou weer uit die databasis teruglees
        $opsies = array();
        foreach($verkiesing->onderwerpe as $onderwerp)
        {
            $opsie_keuse_id = Session::get($verkiesing->id."_".$onderwerp->id);
            $opsie = Opsie::where('id', $opsie_keuse_id)->first();
            $opsies[$onderwerp->id] = $opsie->naam;
        }

      return View::make('bevestig_stembrief')->with(['verkiesing' => $verkiesing, 'onderwerpe' => $verkiesing->onderwerpe, 'opsies' => $opsies]);
    }

  public function saveStem()
  {
    $user = Auth::user();

    //Maak seker daar is 'n oop verkiesing
    if(!Session::has('huidige_verkiesing'))
    {
      return Redirect::to('auth/logout');
    }

    $verkiesing_id = Session::get('huidige_verkiesing');
    $verkiesing = Verkiesing::where('id', $verkiesing_id)->first();

    //Maak seker die persoon het nog nie in die verkiesing gestem nie
    $kieskollege = $verkiesing->kieskollege()->where("user_id", $user->id)->where("verkiesing_id", $verkiesing->id)->first();
    if($kieskollege->pivot->stem === 1)
    {
      return Redirect::to('stem/reedsvoltooi');
    }

    //Check if we have the results in Session
    foreach($verkiesing->onderwerpe as $onderwerp)
    {
     if(!Session::get($verkiesing->id."_".$onderwerp->id))
      return Redirect::to('skakel')->withErrors("Daar was 'n probleem met jou stembrief");
    }

    function gen_uuid() {
      return strtoupper(sprintf( '%04x-%04x-%04x',
          // 32 bits for "time_low"
          mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),

          // 16 bits for "time_mid"
          mt_rand( 0, 0xffff ),

          // 16 bits for "time_hi_and_version",
          // four most significant bits holds version number 4
          mt_rand( 0, 0x0fff ) | 0x4000,

          // 16 bits, 8 bits for "clk_seq_hi_res",
          // 8 bits for "clk_seq_low",
          // two most significant bits holds zero and one for variant DCE1.1
          mt_rand( 0, 0x3fff ) | 0x8000,

          // 48 bits for "node"
          mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
      ));
    };

    $uuid = gen_uuid();

    //Now to save - no User data associated with the save
    foreach($verkiesing->onderwerpe as $onderwerp)
    {
      Log::notice("Saving a User's vote to database. Sensitive info omitted.", ['user_id'=> Auth::user()->id, 'user_name' => Auth::user()->name, 'verkiesing_id' => $verkiesing->id, 'verkiesing_naam' => $verkiesing->naam, 'onderwerp_id' => $onderwerp->id, 'onderwerp_naam' => $onderwerp->naam]);
      $stem = new Stem;
      $stem->verkiesing_id = $verkiesing->id;
      $stem->onderwerp_id = $onderwerp->id;
      $stem->opsie_id = Session::get($verkiesing->id."_".$onderwerp->id);
      $stem->uuid = $uuid;
      $stem->save();
      Log::notice("User's Vote saved to local dabatabase.", ['stem_uuid' => $stem->uuid, 'stem_id'=>$stem->id]);

      //Log::notice("Dispatching a User's vote to the Blockchain", ['stem_uuid' => $stem->uuid, 'stem_id'=>$stem->id]);
      //Write Stem to Blockchain (in the Verkiesing Stream)
      //$job = (new BlockchainCreateNewStreamItem($stem))->delay(5);
      //$this->dispatch($job);

      //Log::notice("Verify that Stem is in Database");

    };

    //update that this user has voted
    Log::notice("User has submitted his vote and will now be marked.", ['user_id'=> Auth::user()->id, 'user_name' => Auth::user()->name, 'verkiesing_id' => $verkiesing->id, 'verkiesing_naam' => $verkiesing->naam]);
    $verkiesing->kieskollege()->updateExistingPivot($user->id, ['stem' => true]);
    $kieskollege->save();
    $user->save();

    $verkiesing->save();

    //Cleanup!
    Session::forget('huidige_verkiesing');

    foreach($verkiesing->onderwerpe as $onderwerp)
    {
      Session::forget($verkiesing->id."_".$onderwerp->id);
    }

    //Logout the user in order to invalidate the current session
    Auth::logout();

    //Sending the UUID of the saved Stem to the view - this is the last time that we can send the UUID to the active session - it has already officially ended in the above line
    return view('stem_gestoor')->with(["uuid" => $stem->uuid, "verkiesing"=> $verkiesing]);
  }

  public function getStemReedsVoltooi()
  {
    Auth::logout(); //Invalidate any future requests until the User starts a new Session
    return view('stem_klaar');
  }

  public function getStemNieBeskikbaar()
  {
    Auth::logout(); //Invalidate any future requests until the User starts a new Session
    return view('stem_nie_beskikbaar');
  }

}
