<?php namespace App\Http\Controllers;

use Input;
use Form;
use Validator;
use Redirect;
use Session;
use Response;
use Log;
use Auth;
use View;
use App\Verkiesing;
use App\User;
use App\Stem;

class AdminController extends Controller {

	public function __construct()
	{
		$this->middleware('auth');
	}

    public function getDashboard()
    {
    	//As daar 'n oop verkiesing is moet 'n paneelbord gewys word
    	if(Verkiesing::where('status', 'oop')->count() == 0)
    	{
    		  //Berekeninge wat op die paneelbord moet gaan
    		  $gereed = Verkiesing::where('status','gereed')->count();
		      $oop = Verkiesing::where('status', 'oop')->count();
		      $gesluit = Verkiesing::where('status', 'toe')->count();
		      $persone = User::where('rol', '=', 'stem')->count();
		      return View::make('paneelbord_geen_oop_verkiesing')->with(['gereed'=>$gereed, 'oop'=>$oop, 'gesluit'=>$gesluit, 'persone'=>$persone]);
    	}
		else
		{
			$verkiesing = Verkiesing::where('status','oop')->first();
			$klaar_gestem = $verkiesing->kieskollege()->wherePivot('stem', true)->count();
			$vordering = round(($klaar_gestem/$verkiesing->kieskollege->count())*100,2);
			$aantal_stemme = Stem::where('verkiesing_id', $verkiesing->id)->count();
			return View::make('paneelbord_oop_verkiesing')->with(['verkiesing' => $verkiesing, 'vordering' => $vordering, 'klaar_gestem' => $klaar_gestem, 'aantal_stemme' => $aantal_stemme]);
		}
      
    }

}