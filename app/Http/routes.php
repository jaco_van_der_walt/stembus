<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return Redirect::to('skakel');
});

Route::get('home', function(){
	return Redirect::to('skakel');
});

//Authenticating
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

Route::get('/skakel', 'BaseController@router');

//Stem
Route::get('stem', 'BaseController@getStem');
Route::post('stem', 'StemController@postStem');

Route::get('stem/suksesvol', 'StemController@saveStem');
Route::get('stem/reedsvoltooi', 'StemController@getStemReedsVoltooi' );
Route::get('stem/geen', 'StemController@getStemNieBeskikbaar');

//Blockchain
Route::get('stem/soek/{id}', 'BlockchainController@getStemSoek');
Route::get('blockchain/info', 'BlockchainController@getBlockchainInfo');

//Hoe Werk Stembus
Route::get('hoewerkstembus', function(){
	return view('hoe_werk_stembus');
});

Route::get('error', function(){
	return view('errors.500');
});

//Route::get('phpinfo', function(){return view("phpinfo");});



// ********* Admin ***********
Route::get('admin', ['middleware' => 'admin.auth', 'uses' => 'AdminController@getDashboard']);

//Persone
Route::get('persone', ['middleware' => 'admin.auth', 'uses' => 'PersoneController@getPersone']);
Route::get('ajax/persone', ['middleware' => 'admin.auth', 'uses' => 'PersoneController@ajax_persone']);
Route::post('persoon/voegby', ['middleware' => 'admin.auth', 'uses' => 'PersoneController@postVoegby']);
Route::get('persoon/skrap/{id}', ['middleware' => 'admin.auth', 'uses' => 'PersoneController@getSkrap']);
Route::get('persone/skrap_almal', ['middleware' => 'admin.auth', 'uses' => 'PersoneController@getSkrapAlmal']);
Route::get('persone/herstel_stemreg', ['middleware' => 'admin.auth', 'uses' => 'PersoneController@getHerstelStemreg']);

Route::get('ajax/persone/kieskollege', ['middleware' => 'admin.auth', 'uses' => 'PersoneController@ajax_kieskollege_persone']);


//Verkiesing
Route::get('verkiesings', ['middleware' => 'admin.auth', 'uses' => 'VerkiesingController@getVerkiesing']);
Route::get('verkiesings/nuut', ['middleware' => 'admin.auth', 'uses' => 'VerkiesingController@getVerkiesingNuut']);
Route::post('verkiesings/nuut', ['middleware' => 'admin.auth', 'uses' => 'VerkiesingController@postVerkiesingNuut']);
Route::get('ajax/verkiesings', ['middleware' => 'admin.auth', 'uses' => 'VerkiesingController@ajax_verkiesings']);
Route::get('verkiesings/skrap/{id}', ['middleware' => 'admin.auth', 'uses' => 'VerkiesingController@getSkrap']);
Route::get('verkiesings/maakoop/{id}', ['middleware' => 'admin.auth', 'uses' => 'VerkiesingController@getMaakOop']);
Route::get('verkiesings/sluit', ['middleware' => 'admin.auth', 'uses' => 'VerkiesingController@getSluit']);
Route::get('verkiesings/resultaat/{id}', ['middleware' => 'admin.auth', 'uses' => 'VerkiesingController@getResultaat']);
Route::get('verkiesings/voorskou/{id}', ['middleware' => 'admin.auth', 'uses' => 'VerkiesingController@getVoorskou']);

Route::get('verkiesings/kieskollege/{id}', ['middleware' => 'admin.auth', 'uses' => 'VerkiesingController@getKieskollege']);
Route::get('stemming/kieskollege/voegby/{id}', ['middleware' => 'admin.auth', 'uses' => 'KieskollegeController@getVoegBy']);
Route::get('ajax/verkiesing/kieskollege/{id}', ['middleware' => 'admin.auth', 'uses' => 'KieskollegeController@getAjaxKieskollege']);
Route::get('kieskollege/verwyder/{id}', ['middleware' => 'admin.auth', 'uses' => 'KieskollegeController@getSkrapPersoon']);
Route::get('boodskap/kieskollege/{id}', ['middleware' => 'admin.auth', 'uses' => 'KieskollegeController@getBoodskap']);
Route::post('boodskap/kieskollege/{id}', ['middleware' => 'admin.auth', 'uses' => 'KieskollegeController@postBoodskap']);

Route::get('kieskollege/table', ['middleware' => 'admin.auth', 'uses' => 'KieskollegeController@getTable']);
Route::get('ajax/kieskolleges', ['middleware' => 'admin.auth', 'uses' => 'KieskollegeController@ajax_kieskolleges']);
Route::post('kieskollege/nuut', ['middleware' => 'admin.auth', 'uses' => 'KieskollegeController@postKieskollegeNuut']);
Route::get('kieskollege/view/{id}', ['middleware' => 'admin.auth', 'uses' => 'KieskollegeController@getViewKieskollege']);
Route::get('ajax/kieskollege/users/{id}', ['middleware' => 'admin.auth', 'uses' => 'KieskollegeController@ajax_kieskollege_users']);
Route::get('ajax/kieskollege/add/users/{id}', ['middleware' => 'admin.auth', 'uses' => 'KieskollegeController@ajax_kieskollege_add_users']);
Route::get('kieskollege/{kieskollege_id}/voeg_by/{user_id}', ['middleware' => 'admin.auth', 'uses' => 'KieskollegeController@getVoegUserByKieskollege']);
Route::get('kieskollege/{kieskollege_id}/verwyder/{user_id}', ['middleware' => 'admin.auth', 'uses' => 'KieskollegeController@getVerwyderUserByKieskollege']);
Route::get('kieskollege/skrap/{id}', ['middleware' => 'admin.auth', 'uses' => 'KieskollegeController@getSkrapKieskollege']);
Route::get('kieskollege/druk/{id}', ['middleware' => 'admin.auth', 'uses' => 'KieskollegeController@drukKieskollege']);
Route::get('ajax/kieskollege/laai/{verkiesing_id}', ['middleware' => 'admin.auth', 'uses' => 'KieskollegeController@getLaaiKieskollege']);
Route::get('kieskollege/{kieskollege_id}/laai/{verkiesing_id}', ['middleware' => 'admin.auth', 'uses' => 'KieskollegeController@getLaaiKieskollegeVerkiesing']);
