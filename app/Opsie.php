<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Opsie extends Model {
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'opsies';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */

    protected $guarded = ['id'];

    public function onderwerp()
    {
        return $this->belongsTo('App\Onderwerp');
    }
}