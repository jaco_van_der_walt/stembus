<?php

namespace App\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Log;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\SomeEvent' => [
            'App\Listeners\EventListener',
        ],
    ];

    /**
     * Register any other events for your application.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher  $events
     * @return void
     */
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);

        $events->listen('auth.attempt', function ($credentials, $remember, $login) {
            Log::info("Login attempt", ['email' => $credentials['email']]);
        });

        $events->listen('auth.login', function ($user, $remember) {
            Log::notice("Login successful", ['user_id'=> $user->id, 'user_name' => $user->name, 'user_email' => $user->email]);
        });

        $events->listen('auth.logout', function ($user) {
            if($user)
            {
                Log::notice("User Logout", ['user_id'=> $user->id, 'user_name' => $user->name, 'user_email' => $user->email]);
            }
        });
    }
}
