<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kieskollege extends Model
{
    protected $table = 'kieskolleges';

    public $timestamps = false;

    public function users()
    {
    	return $this->belongsToMany('App\User', 'users_kieskolleges', 'kieskollege_id', 'user_id');
    }
}
