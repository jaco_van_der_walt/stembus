<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Onderwerp extends Model {
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'onderwerpe';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */

    protected $guarded = ['id'];

    public function opsies()
    {
        return $this->hasMany('App\Opsie');
    }

    public function verkiesing()
    {
        return $this->belongsTo('App\Verkiesing');
    }
}