<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Stem extends Model {
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'stemme';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */

    protected $guarded = ['id'];

    public function opsie()
    {
        return $this->belongsTo('App\Opsie');
    }

    public function onderwerp()
    {
    	return $this->belongsTo('App\Onderwerp');
    }
    
}