<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\User;
use App\Verkiesing;
use Log;

class SendVerkiesingOopSMS extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $sms;
    protected $user;
    protected $verkiesing;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user, Verkiesing $verkiesing)
    {
        $this->sms = new \App\Libraries\MyMobileAPI();
        $this->user = $user;
        $this->verkiesing = $verkiesing;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->sms->sendSms($this->user->selfoon, "Hello ".$this->user->name.". Die stemming op stembus.co.za is oopgemaak. Toegang tot die stembrief met e-posadres ".$this->user->email." en wagwoord ".$this->user->wagwoord." Vir navrae kontak ".$this->verkiesing->admin_selfoon);

        Log::info("SMS Sent: Verkiesing Oop SMS to ".$this->user->selfoon);
    }
}
