<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\User;
use App\Verkiesing;
use Mail;
use Log;

class SendKieskollegeBoodskapEmail extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $user;
    protected $verkiesing;
    protected $boodskap;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user, Verkiesing $verkiesing, $boodskap)
    {
        $this->user = $user;
        $this->verkiesing = $verkiesing;
        $this->boodskap = $boodskap;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::send('emails.boodskap', ['user' => $this->user, 'verkiesing' => $this->verkiesing, 'boodskap' => $this->boodskap], function ($m)
        {
            $m->to($this->user->email, $this->user->name)->subject('Boodskap vanaf '.$this->verkiesing->naam.' administrateur op Stembus');
        });

        Log::info("E-mail Sent: Boodskap email sent to ".$this->user->email);
    }
}
