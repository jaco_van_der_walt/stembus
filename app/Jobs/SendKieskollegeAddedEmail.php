<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\User;
use App\Verkiesing;
use Mail;
use Log;

class SendKieskollegeAddedEmail extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $user;
    protected $verkiesing;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user, Verkiesing $verkiesing)
    {
        $this->user = $user;
        $this->verkiesing = $verkiesing;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::send('emails.persoon_bygevoeg', ['user' => $this->user, 'verkiesing' => $this->verkiesing], function ($m)
        {
            $m->to($this->user->email, $this->user->name)->subject('Jy is geregistreer in '.$this->verkiesing->naam.' op Stembus');
        });

        Log::info("E-mail Sent: Kieskollege Added email sent to ".$this->user->email);
    }
}
