<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\User;
use App\Verkiesing;
use Log;

class SendKieskollegeBoodskapSMS extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $sms;
    protected $user;
    protected $verkiesing;
    protected $boodskap;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user, Verkiesing $verkiesing, $boodskap)
    {
        $this->sms = new \App\Libraries\MyMobileAPI();
        $this->user = $user;
        $this->verkiesing = $verkiesing;
        $this->boodskap = $boodskap;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->sms->sendSms($this->user->selfoon, $this->boodskap);
        Log::info("SMS Sent: Boodskap SMS to ".$this->user->selfoon);
    }
}
