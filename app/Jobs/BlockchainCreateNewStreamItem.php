<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;

use be\kunstmaan\multichain\MultichainClient;
use be\kunstmaan\multichain\MultichainHelper;

use App\Stem;
use App\Verkiesing;
use App\Onderwerp;
use App\Opsie;
use Log;

class BlockchainCreateNewStreamItem extends Job implements SelfHandling, ShouldQueue
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    use InteractsWithQueue, SerializesModels;

    protected $multichain;
    protected $helper;

    protected $stem;

    public function __construct(Stem $stem)
    {
        $this->multichain = new MultichainClient(getenv('JSON_RPC_URL'), getenv('JSON_RPC_USERNAME'), getenv('JSON_RPC_PASSWORD'), 90);
        $this->helper = new MultichainHelper($this->multichain);

        $this->stem = $stem;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        try {
            $verkiesing = Verkiesing::where('id','=',$this->stem->verkiesing_id)->first();
            $onderwerp = Onderwerp::where('id','=',$this->stem->onderwerp_id)->first();
            $opsie = Opsie::where('id','=', $this->stem->opsie_id)->first();
            Log::info("Attempting to write a Vote to the blockchain", ['stem_id'=> $this->stem->id, 'uuid' => $this->stem->uuid]);
            $tx = $this->multichain->publish($verkiesing->stream, $this->stem->uuid, bin2hex("{".$onderwerp->naam.":".$opsie->naam."}"));
            $this->stem->tx = $tx;
            $this->stem->save();
        } catch (Exception $e) {
            Log::error("Unable to write a Vote to blockchain!", ['stem_id' => $this->stem->id, 'uuid' => $this->stem->uuid, 'error'=>$e->getMessage()]);
        } finally {
            if($this->stem->tx != '')
            {
                Log::notice("A Vote was successfully written to the blockchain", ['stem_id' => $this->stem->id, 'uuid' => $this->stem->uuid]);
            }
            else
            {
                Log::warning("A Vote was not properly saved to the Blockchain!", ['stem_id' => $this->stem->id, 'uuid' => $this->stem->uuid]);
                abort(500, "Vote not saved in Blockchain");
            }
        }

        
    }
}
