<?php

namespace App\Jobs;

use App\Verkiesing;
use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;

use be\kunstmaan\multichain\MultichainClient;
use be\kunstmaan\multichain\MultichainHelper;

class BlockchainCreateNewStream extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $multichain;
    protected $helper;

    protected $verkiesing;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Verkiesing $verkiesing)
    {
        $this->multichain = new MultichainClient(getenv('JSON_RPC_URL'), getenv('JSON_RPC_USERNAME'), getenv('JSON_RPC_PASSWORD'), 3);
        $this->helper = new MultichainHelper($this->multichain);

        $this->verkiesing = $verkiesing;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->multichain->createStream($this->verkiesing->stream);
    }
}
