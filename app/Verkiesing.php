<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Verkiesing extends Model {
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'verkiesings';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */

    protected $guarded = ['id'];

    public function onderwerpe()
    {
        return $this->hasMany('App\Onderwerp');
    }

    public function kieskollege()
    {
    	return $this->belongsToMany('App\User', 'users_verkiesings', 'verkiesing_id', 'user_id')->withPivot('stem')->withTrashed();
    }
}