@extends('layouts.master')

@section('head')
@parent
<title>Die Voortrekkers - Stembus</title>

<script src="{{URL('//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css')}}" type="text/css"></script>
@stop


@section('content')
<aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Stemmings
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Stemmings</a></li>
                        <li class="active">Stemmings</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">

                @if($errors->has())
                   @foreach ($errors->all() as $error)
                        <div class="alert alert-danger alert-dismissable">
                            <i class="fa fa-ban"></i>
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <b>Waarskuwing!</b> {{$error}}
                        </div>
                  @endforeach
                @endif

                @if(Session::has('success'))
                        <div class="alert alert-success alert-dismissable">
                            <i class="fa fa-check"></i>
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <b>Sukses!</b> {{Session::get('success')}}
                        </div>
                @endif

                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Stemmings</h3>                                   
                                </div><!-- /.box-header -->

                                <div class="box-body table-responsive">
                                    <table id="verkiesings" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Naam</th>
                                                <th>Stream</th>
                                                <th>Geskep</th>
                                                <th>Status</th>
                                                <th>Kieskollege</th>
                                                <th>Gestem</th>
                                                <th>Stemme</th>
                                                <th>Aksies</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                           
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Naam</th>
                                                <th>Stream</th>
                                                <th>Geskep</th>
                                                <th>Status</th>
                                                <th>Kieskollege</th>
                                                <th>Gestem</th>
                                                <th>Stemme</th>
                                                <th>Aksies</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                    
                                </div><!-- /.box-body -->

                            </div><!-- /.box -->
                            <div>
                                <a href="{{URL('verkiesings/nuut')}}"><button type="button" class="btn btn-success pull-right"><i class='fa fa-plus'></i> Nuwe Stemming</button></a>
                            </div>
                        </div>
                    </div>


                </section><!-- /.content -->
            </aside><!-- /.right-side -->

<!-- Skrap Almal Modal -->
<div id="sluitVerkiesingModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Sluit Verkiesing</h4>
      </div>
      <div class="modal-body">
        Is jy seker jy wil die huidige stemming sluit? As die stemming gesluit is kan dit nie weer oopgemaak word nie en geen verdere stemme sal aanvaar word nie.
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary pull-left" data-dismiss="modal">Kanseleer</button>
        <a href="{{URL('verkiesings/sluit')}}"<button class="btn btn-danger pull-right"><i class="fa fa-lock"></i>  Sluit Stemming</button></a>
      </div>
    </div>

  </div>
</div>



@stop


@section('plugins')
@parent
        <!-- AdminLTE App -->
        <script src="{{URL::asset('assets/admin/js/AdminLTE/app.js')}}" type="text/javascript"></script>

        <!-- DATA TABES SCRIPT -->
        <script src="{{URL('//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
        <script src="{{URL::asset('assets/admin/js/plugins/datatables/dataTables.bootstrap.js')}}" type="text/javascript"></script>

        <!-- AdminLTE App -->
        <script src="{{URL::asset('assets/admin/js/AdminLTE/app.js')}}" type="text/javascript"></script>

        <!-- page script -->
        <script type="text/javascript">
            $(function() {
                $('#verkiesings').DataTable( {
                    "sAjaxSource": "{{{URL('ajax/verkiesings')}}}",
                    "sServerMethod": "GET",
                    "order": [[ 1, "desc" ]]
                } );
                
            });
        </script>


@stop