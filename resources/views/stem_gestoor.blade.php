<!DOCTYPE html>
<html>
    <head>
        <title>Die Voortrekkers - Stembus</title>
        <link rel="shortcut icon" href="{{URL::asset('favicon.png')}}" />

        <!-- Mobile support -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Twitter Bootstrap -->
        <link href="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">

        <!-- Material Design for Bootstrap -->
        <link href="{{URL::asset('assets/dist/css/roboto.min.css')}}" rel="stylesheet">
        <link href="{{URL::asset('assets/dist/css/material-fullpalette.min.css')}}" rel="stylesheet">
        <link href="{{URL::asset('assets/dist/css/ripples.min.css')}}" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="{{URL::asset('assets/dist/css/custom.css')}}" rel="stylesheet">

        <!-- jQuery -->
        <script src="//code.jquery.com/jquery-1.10.2.min.js"></script>
    </head>
    <body>
        <div class="container">
            <div class="content">

                <!-- Begin Content -->
                <div class="bs-docs-section">
                    <div class="row">
                        <div class="col-md-12">

                            <div class="page-header text-center col-md-12">
                                <img src="{{URL::asset('stembus.svg')}}" alt="Stembus Logo" class="col-md-6 col-md-offset-3 max-width-100">
                            </div>

                        </div>
                    </div>
                    <div class="row text-center">
                            <h1>{{$verkiesing->naam}}</h1><br>
                    </div>

                     @if($errors->has())
                       @foreach ($errors->all() as $error)
                            <div class="alert alert-dismissable alert-danger col-lg-8 col-lg-offset-2">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                 <p>{{$error}}</p>
                            </div>
                      @endforeach
                    @endif


                            <div class="row">
                                <div class="col-lg-8 col-lg-offset-2">
                                    <div class="well bs-component text-center">
                                     <h1><i class="mdi-action-done mdi-material-green" style="font-size:90px" ></i></h1>

                                        Dankie! jou stembrief is gestoor met die anonieme stemteken<br>

                                        <h1>{{$uuid}}</h1>
                                        <!-- <div id="blok_status"><span class="blink_me">Stem word in Blokketting geskryf</span></div><br> -->

                                        Hierdie stemteken is bewys van jou stembrief. Hierdie stemteken word nooit met jou naam geasosieer nie en moet privaat gehou word. Hierdie stemteken kan <b>nie</b> later deur die stelsel herroep word nie; maak asseblief 'n nota van die stemteken voordat jy hierdie webblad toemaak.

                                    </div>
                                </div>
                            </div>



                            <div class="col-lg-8 col-lg-offset-2" style="margin-bottom: 40px;">
                                 <a href="{{URL('auth/logout')}}"><button class="btn btn-fab btn-raised btn-material-blue pull-right"><i class="mdi-action-exit-to-app"></i></button></a><br><br>
                            </div>


                </div>
                <!-- End Content -->

            </div>
        </div>




        <!-- Twitter Bootstrap -->
        <script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/js/bootstrap.min.js"></script>

        <!-- Material Design for Bootstrap -->
        <script src="{{URL::asset('assets/dist/js/material.min.js')}}"></script>
        <script src="{{URL::asset('assets/dist/js/ripples.min.js')}}"></script>
        <script>
          $.material.init();

            //Google Analytics
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
              (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
              m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
              })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

              ga('create', 'UA-65569955-1', 'auto');
              ga('send', 'pageview');

            //Poll Blokketting
            /*
            (function poll() {
                setTimeout(function() {
                    $.ajax({
                        url: "{{url('stem/soek', $uuid)}}",
                        type: "GET",
                        success: function(data) {
                            if(data.confirmations > 0 )
                            {
                                $("#blok_status").html("<i>"+data.confirmations + " Blokketting bevestigings</i>");
                            }
                        },
                        dataType: "json",
                        complete: poll,
                        timeout: 5000
                    })
                }, 10000);
            })();*/


        </script>



    </body>
</html>
