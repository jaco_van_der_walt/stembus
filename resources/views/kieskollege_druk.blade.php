<!-- CSS Code: Place this code in the document's head (between the 'head' tags) -->
<style>
table.GeneratedTable {
  width: 100%;
  background-color: #ffffff;
  border-collapse: collapse;
  border-width: 2px;
  border-color: #000000;
  border-style: solid;
  color: #000000;
}

table.GeneratedTable td, table.GeneratedTable th {
  border-width: 2px;
  border-color: #000000;
  border-style: solid;
  padding: 3px;
}

table.GeneratedTable thead {
  background-color: #98c7ff;
}
</style>
<h1>{{$kieskollege->naam}}</h1>
<!-- HTML Code: Place this code in the document's body (between the 'body' tags) where the table should appear -->
<table class="GeneratedTable">
  <thead>
    <tr>
      <th>Naam</th>
      <th>E-posadres</th>
      <th>Selfoon</th>
    </tr>
  </thead>
  <tbody>
    @foreach($kieskollege->users()->orderBy('name','ASC')->get() as $u)
    <tr>
      <td>{{$u->name}}</td>
      <td>{{$u->email}}</td>
      <td>{{$u->selfoon}}</td>
    </tr>
    @endforeach
  </tbody>
</table>
<!-- Codes by Quackit.com -->
<label>Totaal: {{$kieskollege->users()->count()}}</label>
