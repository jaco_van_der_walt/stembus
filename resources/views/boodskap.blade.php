@extends('layouts.master')

@section('head')
@parent
<title>Die Voortrekkers - Stembus</title>
@stop


@section('content')
<aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Boodskap aan Kieskollege
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Stemming</a></li>
                        <li class="active">Resultaat</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">

                @if($errors->has())
                   @foreach ($errors->all() as $error)
                        <div class="alert alert-danger alert-dismissable">
                            <i class="fa fa-ban"></i>
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <b>Waarskuwing!</b> {{$error}}
                        </div>
                  @endforeach
                @endif

                <!-- Small boxes (Stat box) -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-danger">
                            <div class="box-header">
                                <h3 class="box-title"><b>Boodskap</b></h3>
                            </div>
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-12">
                                    <form action="{{url('boodskap/kieskollege', $verkiesing_id)}}" method="POST">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <div class="form-group"> 
                                            <div class="checkbox">
                                                <label>
                                                    <input name="sms" type="checkbox"/>
                                                    SMS
                                                </label>                                                
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input name="epos" type="checkbox"/>
                                                    E-Pos
                                                </label>                                                
                                            </div>
                                        </div>

                                            <div class="form-group">
                                                <label>Boodskap aan Kieskollege:</label>
                                                <textarea class="form-control" rows="3" name="boodskap" id="boodskap"></textarea>
                                                <label class="pull-right">/160</label><label class="pull-right" id="count">0</label>
                                            </div>

                                    </div>
                            
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                    <div class="row">
                        <div class="col-md-12">
                            <button class="btn btn-warning pull-right" type="submit" style="margin-right: 5px;"><i class="fa fa-envelope"></i> Stuur</button>
                        </div>
                    </div>
                    </form>

                </section><!-- /.content -->
            </aside><!-- /.right-side -->


@stop


@section('plugins')
@parent
        <!-- AdminLTE App -->
        <script src="{{URL::asset('assets/admin/js/AdminLTE/app.js')}}" type="text/javascript"></script>
        
        <!-- MakePDF -->
         <script src="{{URL::asset('assets/admin/js/plugins/pdfmake/pdfmake.min.js')}}" type="text/javascript"></script>
         <script src="{{URL::asset('assets/admin/js/plugins/pdfmake/vfs_fonts.js')}}" type="text/javascript"></script>

         <script>
         $("#boodskap").keyup(function(){
              $("#count").text($(this).val().length);
              //console.log($(this).val().length);
            });
         </script>


@stop