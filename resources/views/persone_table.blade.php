@extends('layouts.master')

@section('head')
@parent
<title>Die Voortrekkers - Stembus</title>

<script src="{{URL('//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css')}}" type="text/css"></script>
@stop


@section('content')
<aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Persone
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Persone</a></li>
                        <li class="active">Persone</li>
                    </ol>

                </section>
                <!-- Main content -->
                <section class="content">
                    @if($oop_verkiesing_count != 0)
                        <div class="alert alert-danger alert-dismissable">
                            <i class="fa fa-lock"></i>
                            Daar is tans 'n oop verkiesing. Die stemrooster is gesluit.   
                        </div>
                    @endif

                @if($errors->has())
                   @foreach ($errors->all() as $error)
                        <div class="alert alert-danger alert-dismissable">
                            <i class="fa fa-ban"></i>
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <b>Waarskuwing!</b> {{$error}}
                        </div>
                  @endforeach
                @endif

                @if(Session::has('success'))
                        <div class="alert alert-success alert-dismissable">
                            <i class="fa fa-check"></i>
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <b>Sukses!</b> {{Session::get('success')}}
                        </div>
                @endif

                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Persone</h3>                                   
                                </div><!-- /.box-header -->

                                <div class="box-body table-responsive max-width-100">
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Naam</th>
                                                <th>Voortrekker Registrasie Nommer</th>
                                                <th>Epos Adres</th>
                                                <th>Selfoon</th>
                                                <th>Aksies</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                           
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Naam</th>
                                                <th>Voortrekker Registrasie Nommer</th>
                                                <th>Epos Adres</th>
                                                <th>Selfoon</th>
                                                <th>Aksies</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                    
                                </div><!-- /.box-body -->

                            </div><!-- /.box -->
                            @if($oop_verkiesing_count == 0)
                            <div>
                                <button type="button" class="btn btn-success pull-right" data-toggle="modal" data-target="#addPersoonModal"><i class='fa fa-user-plus'></i> Voeg By</button> 
                            </div>
                            @endif
                        </div>
                    </div>


                </section><!-- /.content -->
            </aside><!-- /.right-side -->

<!-- MODALS -->
<div id="addPersoonModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Voeg Persoon By</h4>
      </div>
      <form class="form-horizontal" action="{{URL('persoon/voegby')}}" method="post">
      <div class="modal-body">
        {!! csrf_field() !!}
         <div class="row form-group">
                <div class="col-xs-4">
                    <label>Naam en Van<span>*</span></label>
                </div>
                <div class="col-xs-8">
                    <input name="naam" id="naam" type="text" class="form-control" required> 
                </div>   
         </div>

        <div class="row form-group">
                <div class="col-xs-4">
                    <label>Voortrekker Registrasie Nommer<span></span></label>
                </div>
                <div class="col-xs-8">
                    <input name="vt_registrasie" id="vt_registrasie" type="text" class="form-control"> 
                </div>   
         </div>

        <div class="row form-group">
                <div class="col-xs-4">
                    <label>E-pos Adres<span>*</span></label>
                </div>
                <div class="col-xs-8">
                    <input name="epos" id="epos" type="email" class="form-control" required> 
                </div>   
         </div>

         <div class="row form-group">
                <div class="col-xs-4">
                    <label>Selfoon Nommer<span>*</span></label>
                </div>
                <div class="col-xs-8">
                    <input name="selfoon" id="selfoon" type="text" class="form-control" required> 
                </div>   
         </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary pull-left" data-dismiss="modal">Cancel</button>
        <button class="btn btn-success pull-right" type="submit"><i class="fa fa-plus"></i>  Voeg By</button>
      </div>
      </form>
    </div>

  </div>
</div>

@stop

@section('plugins')
@parent
        <!-- AdminLTE App -->
        <script src="{{URL::asset('assets/admin/js/AdminLTE/app.js')}}" type="text/javascript"></script>
        <!-- DATA TABES SCRIPT -->
        <script src="{{URL('//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
        <script src="{{URL::asset('assets/admin/js/plugins/datatables/dataTables.bootstrap.js')}}" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="{{URL::asset('assets/admin/js/AdminLTE/app.js')}}" type="text/javascript"></script>
        <!-- page script -->
        <script type="text/javascript">
            $(function() {
                $('#example1').DataTable( {
                    "sAjaxSource": "{{{URL('ajax/persone')}}}",
                    "sServerMethod": "GET"
                } );      
            });
        </script>
@stop