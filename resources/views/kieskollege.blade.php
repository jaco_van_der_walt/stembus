@extends('layouts.master')

@section('head')
@parent
<title>Die Voortrekkers - Stembus</title>

<script src="{{URL('//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css')}}" type="text/css"></script>
@stop


@section('content')
<aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        {{$verkiesing->naam}}: Kieskollege
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Persone</a></li>
                        <li class="active">Kieskollege</li>
                    </ol>

                </section>

                <!-- Main content -->
                <section class="content">

                @if($verkiesing->status == "toe")
                     <div class="alert alert-danger alert-dismissable">
                        <i class="fa fa-lock"></i>
                        Die verkiesing het reeds gesluit. Die Kieskollege kan nie meer verander word nie.
                    </div>
                @endif

                 @if($verkiesing->status == "oop")
                     <div class="alert alert-danger alert-dismissable">
                        <i class="fa fa-lock"></i>
                        Die verkiesing is tans oop. Die Kieskollege kan nie meer verander word nie.
                    </div>
                @endif


                @if($errors->has())
                   @foreach ($errors->all() as $error)
                        <div class="alert alert-danger alert-dismissable">
                            <i class="fa fa-ban"></i>
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <b>Waarskuwing!</b> {{$error}}
                        </div>
                  @endforeach
                @endif

                @if(Session::has('success'))
                        <div class="alert alert-success alert-dismissable">
                            <i class="fa fa-check"></i>
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <b>Sukses!</b> {{Session::get('success')}}
                        </div>
                @endif

                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Persone</h3>
                                </div><!-- /.box-header -->

                                <div class="box-body table-responsive max-width-100">
                                    <table id="kieskollege" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Naam</th>
                                                <th>Epos Adres</th>
                                                <th>Selfoon</th>
                                                <th>Wagwoord</th>
                                                <th>Aksies</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Naam</th>
                                                <th>Epos Adres</th>
                                                <th>Selfoon</th>
                                                <th>Wagwoord</th>
                                                <th>Aksies</th>
                                            </tr>
                                        </tfoot>
                                    </table>

                                </div><!-- /.box-body -->

                            </div><!-- /.box -->

                            <div>
                            @if($verkiesing->status == "gereed")
                                <button type="button" class="btn btn-success pull-right" data-toggle="modal" data-target="#addPersoonModal"><i class='fa fa-user-plus'></i> Voeg By</button>
                                <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#laaiKieskollege" style="margin-right:5px;"><i class='fa fa-file-text-o'></i> Laai Kieskollege</button>
                            @endif
                            </div>

                        </div>
                    </div>


                </section><!-- /.content -->
            </aside><!-- /.right-side -->


<!-- MODALS -->
<div id="addPersoonModal" class="fade modal" role="dialog">
  <div class="modal-dialog" style="width:80%;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Voeg Persoon By</h4>
      </div>
      <div class="modal-body">
        {!! csrf_field() !!}
        <table id="example1" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>Naam</th>
                    <th>Epos Adres</th>
                    <th>Selfoon</th>
                    <th>Aksies</th>
                </tr>
            </thead>
            <tbody>

            </tbody>
            <tfoot>
                <tr>
                    <th>Naam</th>
                    <th>Epos Adres</th>
                    <th>Selfoon</th>
                    <th>Aksies</th>
                </tr>
            </tfoot>
        </table>
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary pull-left" data-dismiss="modal">Cancel</button>
      </div>
    </div>

  </div>
</div>


<!-- MODALS -->
<div id="laaiKieskollege" class="fade modal" role="dialog">
  <div class="modal-dialog" style="width:80%;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Laai Kieskollege</h4>
      </div>
      <div class="modal-body">
        {!! csrf_field() !!}

        <table id="laaiKieskollegeTable" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>Naam</th>
                    <th>Persone</th>
                    <th>Aksies</th>
                </tr>
            </thead>
            <tbody>

            </tbody>
            <tfoot>
                <tr>
                    <th>Naam</th>
                    <th>Persone</th>
                    <th>Aksies</th>
                </tr>
            </tfoot>
        </table>

      </div>
      <div class="modal-footer">
        <button class="btn btn-primary pull-left" data-dismiss="modal">Cancel</button>
      </div>
    </div>

  </div>
</div>


@stop

@section('plugins')
@parent
        <!-- AdminLTE App -->
        <script src="{{URL::asset('assets/admin/js/AdminLTE/app.js')}}" type="text/javascript"></script>

        <!-- DATA TABES SCRIPT -->
        <script src="{{URL('//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
        <script src="{{URL::asset('assets/admin/js/plugins/datatables/dataTables.bootstrap.js')}}" type="text/javascript"></script>

        <!-- AdminLTE App -->
        <script src="{{URL::asset('assets/admin/js/AdminLTE/app.js')}}" type="text/javascript"></script>

        <!-- page script -->
        <script type="text/javascript">

            @if($verkiesing->status == "gereed")
          //Volledige Lys van Persone sodat hulle bygevoeg kan word
          $(function() {
              $('#example1').DataTable( {
                  "sAjaxSource": "{{{URL('ajax/persone/kieskollege')}}}",
                  "sServerMethod": "GET"
              } );

          });

          //Al die gestoorde Kieskolleges wat ingevoer kan word
          $(function() {
              $('#laaiKieskollegeTable').DataTable( {
                  "sAjaxSource": "{{{URL('ajax/kieskollege/laai', $verkiesing->id)}}}",
                  "sServerMethod": "GET"
              } );
          });
          @endif

          //Die bestaande Kieskollege wat reeds in die stemming gelaai is.
          $(function() {
              $('#kieskollege').DataTable( {
                  "sAjaxSource": "{{{URL('ajax/verkiesing/kieskollege',$verkiesing->id)}}}",
                  "sServerMethod": "GET"
              } );

          });

        </script>



@stop
