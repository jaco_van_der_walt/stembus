@extends('layouts.master')

@section('head')
@parent
<title>Die Voortrekkers - Stembus</title>
@stop


@section('content')
<aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Paneelbord
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Paneelbord</a></li>
                        <li class="active">Paneelbord</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">

                    <!-- Small boxes (Stat box) -->
                    <div class="row">
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-aqua">
                                <div class="inner">
                                    <h3>
                                        {{$gereed}}
                                    </h3>
                                    <p>
                                        Stemmings Gereed
                                    </p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-thumbs-o-up"></i>
                                </div>
                                <a href="{{URL('verkiesings')}}" class="small-box-footer">
                                    Meer Inligting <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div><!-- ./col -->
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-green">
                                <div class="inner">
                                    <h3>
                                        {{$oop}}
                                    </h3>
                                    <p>
                                        Stemmings Oop
                                    </p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-unlock"></i>
                                </div>
                                <a href="{{URL('verkiesings')}}" class="small-box-footer">
                                    Meer Inligting <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div><!-- ./col -->
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-red">
                                <div class="inner">
                                    <h3>
                                        {{$gesluit}}
                                    </h3>
                                    <p>
                                        Stemmings Gesluit
                                    </p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-lock"></i>
                                </div>
                                 <a href="{{URL('verkiesings')}}" class="small-box-footer">
                                    Meer Inligting <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div><!-- ./col -->
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-yellow">
                                <div class="inner">
                                    <h3>
                                        {{$persone}}
                                    </h3>
                                    <p>
                                        Persone in Stemregister
                                    </p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-users"></i>
                                </div>
                                 <a href="{{URL('persone')}}" class="small-box-footer">
                                    Meer Inligting <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div><!-- ./col -->

                    </div><!-- /.row -->


                    <!-- top row -->
                    <div class="row">
                        <div class="col-xs-12 connectedSortable">

                        </div><!-- /.col -->
                    </div>
                    <!-- /.row -->

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
@stop


@section('plugins')
@parent

        <!-- AdminLTE App -->
        <script src="{{URL::asset('assets/admin/js/AdminLTE/app.js')}}" type="text/javascript"></script>

@stop
