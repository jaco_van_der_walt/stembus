@extends('layouts.master')

@section('head')
@parent
<title>Die Voortrekkers - Stembus</title>
<meta http-equiv="refresh" content="60">
@stop


@section('content')
<aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Paneelbord
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Paneelbord</a></li>
                        <li class="active">Paneelbord</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-lg-12">
                            <!-- Verkiesing box -->
                            <div class="box box-solid bg-green">
                                <div class="box-header">
                                    <h3 class="box-title"><i class="fa fa-unlock"></i> {{$verkiesing->naam}}</h3>
                                </div>
                                <div class="box-body">
                                    <p>
                                        {{$verkiesing->beskrywing}}
                                    </p>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
                    </div>


                    <!-- Small boxes (Stat box) -->
                    <div class="row">
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-green">
                                <div class="inner">
                                    <h3>
                                        Begin
                                    </h3>

                                    <p>
                                        {{$verkiesing->oopgemaak}}
                                    </p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-clock-o"></i>
                                </div>
                                <a href="{{URL('verkiesings')}}" class="small-box-footer">
                                    Meer Inligting <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div><!-- ./col -->

                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-yellow">
                                <div class="inner">
                                    <h3>
                                        {{$verkiesing->kieskollege()->count()}}
                                    </h3>
                                    <p>
                                        Persone in Kieskollege
                                    </p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-users"></i>
                                </div>
                                 <a href="{{URL('persone')}}" class="small-box-footer">
                                    Meer Inligting <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div><!-- ./col -->

                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-aqua">
                                <div class="inner">
                                    <h3>
                                        {{$klaar_gestem}}
                                    </h3>
                                    <p>
                                        Persone klaar gestem
                                    </p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-users"></i>
                                </div>
                                 <a href="{{URL('persone')}}" class="small-box-footer">
                                    Meer Inligting <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div><!-- ./col -->
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-maroon">
                                <div class="inner">
                                    <h3>
                                        {{$aantal_stemme}}
                                    </h3>
                                    <p>
                                        Stemme ontvang
                                    </p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-flag"></i>
                                </div>
                                <a href="{{URL('verkiesings')}}" class="small-box-footer">
                                    Meer Inligting <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div><!-- ./col -->
                    </div><!-- /.row -->

                    <!-- top row -->
                    <div class="row">
                        <div class="col-xs-12 connectedSortable">

                        </div><!-- /.col -->
                    </div>
                    <!-- /.row -->

                    <!-- Main row -->
                    <div class="row">
                        <!-- Left col -->
                        <section class="col-lg-12">
                            <!-- Box (with bar chart) -->
                            <div class="box box-info" id="loading-example">
                                <div class="box-header">
                                    <!-- tools box -->
                                    <i class="fa fa-line-chart"></i>

                                    <h3 class="box-title">Vordering %</h3>
                                </div><!-- /.box-header -->
                                <div class="box-body no-padding">
                                    <div class="row">
                                        <div class="col-md-12 text-center">
                                            <input type="text" class="knob" data-readonly="true" value="{{$vordering}}" data-width="180" data-height="180" data-fgColor="#367FA9"/>
                                            <div>
                                                <h4><b>%</b></h4>
                                            </div>
                                        </div><!-- ./col -->
                                    </div><!-- /.row -->
                                </div><!-- /.box-footer -->
                            </div><!-- /.box -->


                        </section><!-- /.Left col -->
                        <!-- right col (We are only adding the ID to make the widgets sortable)-->

                    </div><!-- /.row (main row) -->

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
@stop


@section('plugins')
@parent

        <!-- jQuery Knob Chart -->
        <script src="{{URL::asset('assets/admin/js/plugins/jqueryKnob/jquery.knob.js')}}" type="text/javascript"></script>

        <!-- AdminLTE App -->
        <script src="{{URL::asset('assets/admin/js/AdminLTE/app.js')}}" type="text/javascript"></script>

        <!-- Page script -->
        <script type="text/javascript">
                /* jQueryKnob */
                $(".knob").knob();
        </script>

@stop
