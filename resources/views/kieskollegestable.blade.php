@extends('layouts.master')

@section('head')
@parent
<title>Die Voortrekkers - Stembus</title>

<script src="{{URL('//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css')}}" type="text/css"></script>
@stop


@section('content')
<aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Kieskolleges
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Kieskolleges</a></li>
                    </ol>

                </section>

                <!-- Main content -->
                <section class="content">


                @if($errors->has())
                   @foreach ($errors->all() as $error)
                        <div class="alert alert-danger alert-dismissable">
                            <i class="fa fa-ban"></i>
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <b>Waarskuwing!</b> {{$error}}
                        </div>
                  @endforeach
                @endif

                @if(Session::has('success'))
                        <div class="alert alert-success alert-dismissable">
                            <i class="fa fa-check"></i>
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <b>Sukses!</b> {{Session::get('success')}}
                        </div>
                @endif

                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Kieskolleges</h3>                                   
                                </div><!-- /.box-header -->

                                <div class="box-body table-responsive max-width-100">
                                        <table id="kieskolleges" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Naam</th>
                                                <th>Persone</th>
                                                <th>Aksies</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                           
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Naam</th>
                                                <th>Persone</th>
                                                <th>Aksies</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                    
                                </div><!-- /.box-body -->

                            </div><!-- /.box -->

                            <div>
                                <button type="button" class="btn btn-success pull-right" data-toggle="modal" data-target="#addKieskollegeModal"><i class='fa fa-user-plus'></i> Nuwe Kieskollege</button> 
                            </div>

                        </div>
                    </div>


                </section><!-- /.content -->
            </aside><!-- /.right-side -->



<!-- MODALS -->
<div id="addKieskollegeModal" class="fade modal" role="dialog">
  <div class="modal-dialog" style="width:50%;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Nuwe Kieskollege</h4>
      </div>
      <div class="modal-body">
        <form role="form" action="{{URL('kieskollege/nuut')}}" method="POST">
        {!! csrf_field() !!}
            <!-- Naam -->
            <div class="row form-group">
                <div class="col-xs-2">
                    <label>Kieskollege Naam <span>*</span></label>
                </div>
                <div class="col-xs-4">
                    <input type="text" class="form-control" name="kieskollege_naam" value="{{Input::old('kieskollege_naam')}}" required> 
                </div>   
            </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary pull-left" data-dismiss="modal">Cancel</button>
        <button type="submit" class="btn btn-success pull-right">Skep</button>
      </div>
    </div>
    </form>
  </div>
</div>




@stop

@section('plugins')
@parent
        <!-- AdminLTE App -->
        <script src="{{URL::asset('assets/admin/js/AdminLTE/app.js')}}" type="text/javascript"></script>

        <!-- DATA TABES SCRIPT -->
        <script src="{{URL('//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
        <script src="{{URL::asset('assets/admin/js/plugins/datatables/dataTables.bootstrap.js')}}" type="text/javascript"></script>

        <!-- AdminLTE App -->
        <script src="{{URL::asset('assets/admin/js/AdminLTE/app.js')}}" type="text/javascript"></script>

        <!-- page script -->
        <script type="text/javascript">

            $(function() {
                $('#kieskolleges').DataTable( {
                    "sAjaxSource": "{{{URL('ajax/kieskolleges')}}}",
                    "sServerMethod": "GET"
                } );
                
            });
        </script>



@stop