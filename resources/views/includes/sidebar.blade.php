                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="{{URL::asset('assets/admin/img/stembus_icon.png')}}" class="img-circle" alt="User Image" />
                        </div>
                        <div class="pull-left info">
                            <p>{{{ isset(Auth::user()->name) ? Auth::user()->name : Auth::user()->email }}}</p>

                            <a href="#"><i class="fa fa-circle text-success"></i> Aanlyn</a>
                        </div>
                    </div>
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li class="active">
                            <a href="{{URL('/')}}">
                                <i class="fa fa-dashboard"></i> <span>Paneelbord</span>
                            </a>
                        </li>

                        <li @if($active = Request::is('verkiesings/*')) class="active" @endif>
                            <a href="{{{URL('verkiesings')}}}">
                                <i class="fa fa-th-list"></i>
                                <span>Stemmings</span>
                                
                            </a>
                        </li>

                        <li @if($active = Request::is('kieskollege/*')) class="active" @endif>
                            <a href="{{{URL('kieskollege/table')}}}">
                                <i class="fa fa-file-text-o"></i>
                                <span>Kieskolleges</span>
                            </a>
                        </li>


                        <li @if($active = Request::is('persone/*')) class="active" @endif>
                            <a href="{{{URL('persone')}}}">
                                <i class="fa fa-users"></i>
                                <span>Persone</span>
                                
                            </a>
                        </li>
                    </ul>
                </section>
                <!-- /.sidebar -->