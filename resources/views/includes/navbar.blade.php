            <a href="{{URL('home')}}" class="logo">
                <!-- Add the class icon to your logo image or logo icon to add the margining -->
                Stembus Admin
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-right">
                    <ul class="nav navbar-nav">

                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="glyphicon glyphicon-user"></i>
                                <span>{{{ isset(Auth::user()->name) ? Auth::user()->name : Auth::user()->email }}}<i class="caret"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header bg-light-blue">
                                    <img src="{{URL::asset('assets/admin/img/stembus_icon.png')}}" class="img-circle" alt="User Image" />
                                    <p>
                                        {{{ isset(Auth::user()->name) ? Auth::user()->name : Auth::user()->email }}}
                                        <small>Stembus Administrateur</small>
                                    </p>
                                </li>
                                </li>
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-right">
                                        <a href="{{url('/auth/logout')}}" class="btn btn-default btn-flat">Teken Uit</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>