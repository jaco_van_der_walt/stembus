<!DOCTYPE html>
<html>
    <head>
        <title>Die Voortrekkers - Stembus</title>
        <link rel="shortcut icon" href="{{URL::asset('favicon.png')}}" />

        <!-- Mobile support -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Outdated Browser -->
        <link rel="stylesheet" href="{{URL::asset('assets/scripts/outdatedbrowser/outdatedbrowser.min.css')}}">

        <!-- Twitter Bootstrap -->
        <link href="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">

        <!-- Material Design for Bootstrap -->
        <link href="{{URL::asset('assets/dist/css/roboto.min.css')}}" rel="stylesheet">
        <link href="{{URL::asset('assets/dist/css/material-fullpalette.min.css')}}" rel="stylesheet">
        <link href="{{URL::asset('assets/dist/css/ripples.min.css')}}" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="{{URL::asset('assets/dist/css/custom.css')}}" rel="stylesheet">

        <!-- jQuery -->
        <script src="//code.jquery.com/jquery-1.10.2.min.js"></script>
    </head>
    <body>
        <div class="container">
            <div class="content">

                <!-- Begin Content -->
                <div class="bs-docs-section">
                    <div class="row">
                        <div class="col-md-12">

                            <div class="page-header text-center col-md-12">
                                <img src="{{URL::asset('stembus.svg')}}" alt="Stembus Logo" class="col-md-6 col-md-offset-3 max-width-100">
                            </div>

                        </div>
                    </div>

                     @if($errors->has())
                       @foreach ($errors->all() as $error)
                            <div class="alert alert-dismissable alert-danger col-lg-8 col-lg-offset-2">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                 <p>{{$error}}</p>
                            </div>
                      @endforeach
                    @endif

                    <div class="row">
                        <div class="col-lg-8 col-lg-offset-2">
                            <div class="well bs-component">
                                <form class="form-horizontal" action="{{URL('auth/login')}}" method="post">
                                    <fieldset>
                                        <legend>Teken In</legend>
                                        {{ csrf_field() }}
                                        <div class="form-group">
                                            <label for="email" class="col-lg-2 control-label">E-pos</label>
                                            <div class="col-lg-10">
                                                <input type="email" class="form-control" id="email" name="email" placeholder="E-posadres" value="{{old('email')}}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="password" class="col-lg-2 control-label">Wagwoord</label>
                                            <div class="col-lg-10">
                                                <input type="password" class="form-control" id="password" name="password" placeholder="Wagwoord">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-lg-10 col-lg-offset-2">
                                                <button type="submit" class="btn btn-primary pull-right">Teken In</button>
                                            </div>
                                        </div>
                                    </fieldset>
                                </form>
                            </div>
                            <div class="text-center">
                                <br>
                                <a href="{{URL('hoewerkstembus')}}">Hoe werk stembus?</a>
                            </div>

                            <div class="row">
                                <div class="col-md-12">

                                    <div class="text-center col-md-12">
                                        <!--<img src="{{URL::asset('DameskringLogo.PNG')}}" alt="Stembus Logo" class="col-md-4 col-xs-6 col-xs-offset-3 col-md-offset-4 max-width-100" style="margin-top:10px;"> -->
                                    </div>

                                </div>
                            </div>

                        </div>

                    </div>
                </div>
                <!-- End Content -->

            </div>
        </div>

        <!-- ============= Outdated Browser ============= -->
        <div id="outdated"></div>


        <script src="{{URL::asset('assets/scripts/outdatedbrowser/outdatedbrowser.min.js')}}"></script>

        <script>
            //event listener: DOM ready
            function addLoadEvent(func) {
                var oldonload = window.onload;
                if (typeof window.onload != 'function') {
                    window.onload = func;
                } else {
                    window.onload = function() {
                        if (oldonload) {
                            oldonload();
                        }
                        func();
                    }
                }
            }
            //call plugin function after DOM ready
            addLoadEvent(function(){
                outdatedBrowser({
                    bgColor: '#f25648',
                    color: '#ffffff',
                    lowerThan: 'transform',
                    languagePath: "{{URL::asset('assets/scripts/outdatedbrowser/lang/en.html')}}"
                })
            });
        </script>


        <!-- Twitter Bootstrap -->
        <script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/js/bootstrap.min.js"></script>

        <!-- Material Design for Bootstrap -->
        <script src="{{URL::asset('assets/dist/js/material.min.js')}}"></script>
        <script src="{{URL::asset('assets/dist/js/ripples.min.js')}}"></script>
        <script>
          $.material.init();

            //Google Analytics
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
              (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
              m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
              })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

              ga('create', 'UA-65569955-1', 'auto');
              ga('send', 'pageview');


        </script>



    </body>
</html>
