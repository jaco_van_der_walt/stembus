@extends('layouts.master')

@section('head')
@parent
<title>Die Voortrekkers - Stembus</title>
@stop


@section('content')
<aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Verkiesing
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Verkiesing</a></li>
                        <li class="active">Voeg By</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">

                @if($errors->has())
                   @foreach ($errors->all() as $error)
                        <div class="alert alert-danger alert-dismissable">
                            <i class="fa fa-ban"></i>
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <b>Waarskuwing!</b> {{$error}}
                        </div>
                  @endforeach
                @endif


                    <!-- Small boxes (Stat box) -->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- form start -->
                           <form role="form" action="{{URL('verkiesings/nuut')}}" method="POST">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title"><b>Stemming Inligting</b></h3>
                                </div><!-- /.box-header -->
                                
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                
                                    <div class="box-body">

                                        <!-- Naam -->
                                        <div class="row form-group">
                                            <div class="col-xs-2">
                                                <label>Stemming Naam <span>*</span></label>
                                            </div>
                                            <div class="col-xs-4">
                                                <input type="text" class="form-control" name="verkiesing_naam" value="{{Input::old('verkiesing_naam')}}" required> 
                                            </div>   
                                        </div>

                                    <!-- Beskrywing -->
                                        <div class="row form-group">
                                             <div class="col-xs-2">
                                                <label>Stemming Beskrywing <span>*</span></label>
                                            </div>
                                            <div class="col-xs-4">
                                               <textarea id="verkiesing_beskrywing" name="verkiesing_beskrywing" class="form-control" rows="4" required></textarea> 
                                            </div>   
                                        </div>

                                        <!-- Admin Naam -->
                                        <div class="row form-group">
                                            <div class="col-xs-2">
                                                <label>Admin Naam <span>*</span></label>
                                            </div>
                                            <div class="col-xs-4">
                                                <input type="text" class="form-control" name="admin_naam" value="{{Input::old('admin_naam')}}" required> 
                                            </div>   
                                        </div>

                                        <!-- Admin Naam -->
                                        <div class="row form-group">
                                            <div class="col-xs-2">
                                                <label>Admin Selfoon <span>*</span></label>
                                            </div>
                                            <div class="col-xs-4">
                                                <input type="text" class="form-control" name="admin_selfoon" value="{{Input::old('admin_selfoon')}}" required> 
                                            </div>   
                                        </div>
                                    
                                    </div>
                                
                            </div><!-- /.box --> 

                            <!-- Onderwerpe en Opsies --> 
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title"><b>Kennisgewings</b></h3>
                                </div><!-- /.box-header -->
                                <div class="box-body">
                                        <!-- EposByvoeg -->
                                        <div class="row form-group">
                                            <div class="col-xs-4">
                                                <input name="epos_bygevoeg" type="checkbox" checked/>
                                                    <b>E-pos</b> wanneer 'n persoon by <b>Stemming</b> gevoeg word
                                            </div>   
                                        </div>
                                        <!-- EposOopmaak -->
                                        <div class="row form-group">
                                            <div class="col-xs-4">
                                                <input name="epos_oopmaak" type="checkbox" checked/>
                                                    <b>E-pos</b> wanneer die <b>Stemming oopmaak</b>
                                            </div>   
                                        </div>
                                        <!-- SMSOopmaak -->
                                        <div class="row form-group">
                                            <div class="col-xs-4">
                                                <input name="sms_oopmaak" type="checkbox" checked/>
                                                    <b>SMS</b> wanneer die <b>Stemming oopmaak</b>
                                            </div>   
                                        </div>
                                </div>
                            </div>

                            <!-- Onderwerpe en Opsies --> 
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title"><b>Onderwerpe & Opsies</b></h3>
                                </div><!-- /.box-header -->
                                <div id="onderwerpe_opsies_box" class="box-body">
                                    <div class="row form-group">
                                        <div class="col-xs-2">
                                            <button class="btn btn-success" style="margin-right: 5px;" data-toggle="modal" data-target="#addOnderwerpOpsieModal"><i class="fa fa-plus"></i>  Nuwe Onderwerp</button>  
                                            </div>
                                    </div>
                                </div>
                            </div>

                            <div class="box-footer">
                                <button type="submit" class="btn btn-success pull-right" style="margin-right: 5px;"> Voeg by</button>
                            </div>

                            </form>
                    </div><!-- /.row (main row) -->

                </section><!-- /.content -->
            </aside><!-- /.right-side -->




<!-- MODALS -->
<div id="addOnderwerpOpsieModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Nuwe Onderwerp</h4>
      </div>
      <div class="modal-body">
         <div class="row form-group">
                <div class="col-xs-4">
                    <label>Onderwerp <span>*</span></label>
                </div>
                <div class="col-xs-8">
                    <input id="new_onderwerp" type="text" class="form-control"> 
                </div>   
         </div>

         <div class="row form-grouo">
                <div class="col-xs-4">
                    <label>Opsies <span>*</span></label>
                    <br>
                    <p><i>Een opsie per lyn</i></p>  
                    <br>
                    <p><i>Spesiale karakters soos <b>',(?</b> breek soms die stembrief. Vermy dit asseblief.</i></p>                 
                </div>
                <div class="col-xs-8">
                    <textarea id="new_opsies" class="form-control" rows="4"></textarea>
                </div>
         </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary pull-left" data-dismiss="modal">Cancel</button>
        <a href="#" onclick=validate()><button class="btn btn-success pull-right"><i class="fa  fa-plus"></i>  Voeg By</button></a>
      </div>
    </div>

  </div>
</div>









@stop


@section('plugins')
@parent
        <!-- AdminLTE App -->
        <script src="{{URL::asset('assets/js/AdminLTE/app.js')}}" type="text/javascript"></script>

        <script type="text/javascript">

            function addOpsieOnderwerp()
            {
                var onderwerp = $("#new_onderwerp").val();



                var opsies = $("#new_opsies").val();
                var lines = opsies.split(/\n/); 
                var retext = "";
                for(var i in lines) {
                    retext += lines[i] + "<br>";
                }
                $("#new_onderwerp").val("");
                $("#new_opsies").val("");
                //Insert the data from the Modal onto the Page, including hidden HTML input tags containing the Form data
                $("#onderwerpe_opsies_box").append("<div class='row form-group'><div class='col-md-4'><div class='box box-solid bg-light-blue'><div class='box-header'><h3 class='box-title'>" + onderwerp + "</h3><button type='button' style='margin-top: 3px; margin-right: 3px;' class='btn btn-danger pull-right' onclick=deleteAlert(this)><i class='fa  fa-trash-o'></i></button></div><div class='box-body'><p>"+ retext +"</p><input name='onderwerpe[]' value='"+onderwerp+"' type='text' hidden><input name='opsies[]' value='"+lines+"' type='text' hidden></div></div></div></div>");
            }

            //Delete the entire element (including hidden divs)
            function deleteAlert(_elem)
            {
                $(_elem).closest('.row').remove();
            }

            function validate()
            {
                if($("#new_onderwerp").val() === "")
                    {return;}
                else if($("#new_opsies").val() === "")
                    {return;}
                else{
                    addOpsieOnderwerp();
                    $('#addOnderwerpOpsieModal').modal('toggle');
                };
 
            }
        </script> 


@stop