<!DOCTYPE html>
<html>
    <head>
        <title>Die Voortrekkers - Stembus</title>
        <link rel="shortcut icon" href="{{URL::asset('favicon.png')}}" />

        <!-- Mobile support -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Twitter Bootstrap -->
        <link href="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">

        <!-- Material Design for Bootstrap -->
        <link href="{{URL::asset('assets/dist/css/roboto.min.css')}}" rel="stylesheet">
        <link href="{{URL::asset('assets/dist/css/material-fullpalette.min.css')}}" rel="stylesheet">
        <link href="{{URL::asset('assets/dist/css/ripples.min.css')}}" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="{{URL::asset('assets/dist/css/custom.css')}}" rel="stylesheet">

        <!-- jQuery -->
        <script src="//code.jquery.com/jquery-1.10.2.min.js"></script>
    </head>
    <body>
        <div class="container">
            <div class="content">
            
                <!-- Begin Content -->
                <div class="bs-docs-section">

                     @if($errors->has())
                       @foreach ($errors->all() as $error)
                            <div class="alert alert-dismissable alert-danger col-lg-8 col-lg-offset-2">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                 <p>{{$error}}</p> 
                            </div>
                      @endforeach
                    @endif


                            <div class="row">
                                <div class="col-lg-8 col-lg-offset-2">
                                         <img src="{{URL::asset('assets/images/whoops.svg')}}" alt="whoops" class="max-width-100">              
                                </div>
                            </div>
                            


                       <div class="col-lg-8 col-lg-offset-2" style="margin-bottom: 40px;">
                                 <a href="{{URL('auth/logout')}}"><button class="btn btn-fab btn-raised btn-material-blue pull-right"><i class="mdi-action-exit-to-app"></i></button></a><br><br>
                        </div>
                    
                </div>
                <!-- End Content -->

            </div>
        </div>




        <!-- Twitter Bootstrap -->
        <script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/js/bootstrap.min.js"></script>

        <!-- Material Design for Bootstrap -->
        <script src="{{URL::asset('assets/dist/js/material.min.js')}}"></script>
        <script src="{{URL::asset('assets/dist/js/ripples.min.js')}}"></script>
        <script>
          $.material.init();
        </script>



    </body>
</html>
