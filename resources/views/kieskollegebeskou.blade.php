@extends('layouts.master')

@section('head')
@parent
<title>Die Voortrekkers - Stembus</title>

<script src="{{URL('//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css')}}" type="text/css"></script>
@stop


@section('content')
<aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Kieskollege: {{$kieskollege->naam}}
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Kieskolleges</a></li>
                    </ol>

                </section>

                <!-- Main content -->
                <section class="content">


                @if($errors->has())
                   @foreach ($errors->all() as $error)
                        <div class="alert alert-danger alert-dismissable">
                            <i class="fa fa-ban"></i>
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <b>Waarskuwing!</b> {{$error}}
                        </div>
                  @endforeach
                @endif

                @if(Session::has('success'))
                        <div class="alert alert-success alert-dismissable">
                            <i class="fa fa-check"></i>
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <b>Sukses!</b> {{Session::get('success')}}
                        </div>
                @endif

                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">{{$kieskollege->naam}}</h3>
                                </div><!-- /.box-header -->

                                <div class="box-body table-responsive max-width-100">
                                        <table id="kieskolleges" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Naam</th>
                                                <th>E-Pos</th>
                                                <th>Selfoon</th>
                                                <th>Aksies</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Naam</th>
                                                <th>E-Pos</th>
                                                <th>Selfoon</th>
                                                <th>Aksies</th>
                                            </tr>
                                        </tfoot>
                                    </table>

                                </div><!-- /.box-body -->

                            </div><!-- /.box -->

                            <div>
                                <button type="button" class="btn btn-danger pull-left" data-toggle="modal" data-target="#skrapKieskollege"><i class='fa fa-trash'></i> Skrap Kieskollege</button>&nbsp
                                <a type="button" class="btn btn-info pull-left" style="margin-left:10px;" href="{{url('kieskollege/druk', $kieskollege->id)}}"><i class='fa fa-print'></i> Druk Kieskollege</a>
                                <button type="button" class="btn btn-success pull-right" data-toggle="modal" data-target="#addPersoonModal"><i class='fa fa-user-plus'></i> Voeg persoon by</button>
                            </div>

                        </div>
                    </div>


                </section><!-- /.content -->
            </aside><!-- /.right-side -->



<!-- MODALS -->
<div id="addPersoonModal" class="fade modal" role="dialog">
  <div class="modal-dialog" style="width:80%;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Voeg Persoon By</h4>
      </div>
      <div class="modal-body">
        {!! csrf_field() !!}

        <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Naam</th>
                                                <th>Epos Adres</th>
                                                <th>Selfoon</th>
                                                <th>Aksies</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Naam</th>
                                                <th>Epos Adres</th>
                                                <th>Selfoon</th>
                                                <th>Aksies</th>
                                            </tr>
                                        </tfoot>
                                    </table>

      </div>
      <div class="modal-footer">
        <button class="btn btn-primary pull-left" data-dismiss="modal">Cancel</button>
      </div>
    </div>

  </div>
</div>


<!-- MODALS -->
<div id="skrapKieskollege" class="fade modal" role="dialog">
  <div class="modal-dialog" style="width:30%;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Skrap {{$kieskollege->naam}}</h4>
      </div>
      <div class="modal-body">
      <p>Is jy seker dat jy {{$kieskollege->naam}} wil skrap? Alle persone sal uit die Kieskollege verwyder word maar die persoon sal nie geskrap word nie.
      </p>

      </div>
      <div class="modal-footer">
        <a href="{{url('kieskollege/skrap',$kieskollege->id)}}" class="btn btn-danger pull-left">Skrap Kieskollege</a>
        <button class="btn btn-primary pull-right" data-dismiss="modal">Cancel</button>
      </div>
    </div>

  </div>
</div>


@stop

@section('plugins')
@parent
        <!-- AdminLTE App -->
        <script src="{{URL::asset('assets/admin/js/AdminLTE/app.js')}}" type="text/javascript"></script>

        <!-- DATA TABES SCRIPT -->
        <script src="{{URL('//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
        <script src="{{URL::asset('assets/admin/js/plugins/datatables/dataTables.bootstrap.js')}}" type="text/javascript"></script>

        <!-- AdminLTE App -->
        <script src="{{URL::asset('assets/admin/js/AdminLTE/app.js')}}" type="text/javascript"></script>

        <!-- page script -->
        <script type="text/javascript">

            $(function() {
                $('#kieskolleges').DataTable( {
                    "sAjaxSource": "{{{url('ajax/kieskollege/users',$kieskollege->id)}}}",
                    "sServerMethod": "GET"
                } );

            });

            $(function() {
                $('#example1').DataTable( {
                    "sAjaxSource": "{{{URL('ajax/kieskollege/add/users',$kieskollege->id)}}}",
                    "sServerMethod": "GET"
                } );

            });
        </script>



@stop
