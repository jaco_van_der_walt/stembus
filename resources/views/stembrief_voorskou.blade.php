<!DOCTYPE html>
<html>
    <head>
        <title>Die Voortrekkers - Stembus</title>
        <link rel="shortcut icon" href="{{URL::asset('favicon.png')}}" />

        <!-- Mobile support -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Twitter Bootstrap -->
        <link href="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
        <!-- Material Design for Bootstrap -->
        <link href="{{URL::asset('assets/dist/css/roboto.min.css')}}" rel="stylesheet">
        <link href="{{URL::asset('assets/dist/css/material-fullpalette.min.css')}}" rel="stylesheet">
        <link href="{{URL::asset('assets/dist/css/ripples.min.css')}}" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="{{URL::asset('assets/dist/css/custom.css')}}" rel="stylesheet">
        <!-- jQuery -->
        <script src="//code.jquery.com/jquery-1.10.2.min.js"></script>
    </head>
    <body>
        <div class="container">
            <div class="content">
            
                <!-- Begin Content -->
                <div class="bs-docs-section">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="page-header text-center">
                                <div class="alert alert-dismissable alert-warning col-lg-12">
                                    <p>Hierdie is slegs 'n voorskou van die stembrief. Dit is nie 'n oop stemming nie en geen stemme sal gestoor word nie.</p> 
                                </div>
                                <div class="alert alert-dismissable alert-info col-lg-12">
                                    <p>Die Kieskollege sal die volgende kennisgewings ontvang:
                                    @if($verkiesing->epos_bygevoeg == '1')
                                    <br>‣ 'n <b>e-pos</b> sodra hulle by die Stemming gevoeg word
                                    @endif
                                    @if($verkiesing->epos_oopmaak == '1')
                                    <br>‣ 'n <b>e-pos</b> sodra die Stemming oopgemaak word
                                    @endif
                                    @if($verkiesing->sms_oopmaak == '1')
                                    <br>‣ 'n <b>SMS</b> sodra die Stemming oopgemaak word
                                    @endif
                                    @if(($verkiesing->epos_bygevoeg == '0') && ($verkiesing->epos_oopmaak == '0') && ($verkiesing->sms_oopmaak == '0'))
                                    <br> <b>Geen</b>
                                    @endif

                                    </p> 
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
    
                            <div class="page-header text-center col-md-12">
                                <img src="{{URL::asset('stembus.svg')}}" alt="Stembus Logo" class="col-md-6 col-md-offset-3 max-width-100">
                            </div>
                            
                        </div>
                    </div>
                    <div class="row text-center">
                            <h1>{{$verkiesing->naam}}</h1>
                            <br>
                    </div>
                    <div class="row text-center">
                        {!! $verkiesing->beskrywing !!}
                        <br><br>
                    </div>
                    <form class="form-horizontal" action="#" method="post">
                        <fieldset>

                            @foreach($onderwerpe as $onderwerp)

                            <div class="row">
                                <div class="col-lg-8 col-lg-offset-2">
                                    <div class="well bs-component">
                                                <legend>{{$onderwerp->naam}}</legend>
                                                <p>{{$onderwerp->beskrywing}}</p>
                                                <div class="form-group">
                                                    <div class="col-lg-10">
                                                        @foreach($opsies[$onderwerp->id] as $opsie)
                                                            <div class="radio radio-primary">
                                                            <label>
                                                                <input type="radio" name="{{$onderwerp->id}}" id="{{$opsie->id}}" value="{{$opsie->id}}" required>
                                                                {{$opsie->naam}}
                                                            </label>
                                                        </div>

                                                    
                                                        @endforeach

                                                    </div>
                                                </div>
                                    </div>
                                </div>
                            </div>

                            @endforeach

                        </fieldset>
                    </form>
              
                </div>
                <!-- End Content -->
            </div>
        </div>

        <!-- Twitter Bootstrap -->
        <script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/js/bootstrap.min.js"></script>

        <!-- Material Design for Bootstrap -->
        <script src="{{URL::asset('assets/dist/js/material.min.js')}}"></script>
        <script src="{{URL::asset('assets/dist/js/ripples.min.js')}}"></script>
        <script>
          $.material.init();
        </script>



    </body>
</html>
