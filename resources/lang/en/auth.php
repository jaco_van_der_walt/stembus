<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Hierdie e-posadres en wagwoord kombinasie is nie geldig nie.',
    'throttle' => "Te veel onsuksesvolle aanteken pogings. Wag asseblief 'n paar sekondes en probeer weer",

];
